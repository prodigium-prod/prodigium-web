@extends("front.layouts.master")

<!-- Set Title -->
@section('title', config('app.name')." | ".__("words.pages.contact-us.title"))

@push('styles')
    <!-- Add custom styles  -->
@endpush
@section("content")
    <!--------------------------------------------- Contact  -------------------------------------------->
    <div class="container-fluid contact_us_bg_img">
        <div class="container">
            <div class="text-center">
                <a href="#" class="fh5co_con"> {{ __("words.pages.contact-us.title") }} </a>
            </div>
        </div>
    </div>
    
    <div class="container-fluid  fh5co_fh5co_bg_contcat">
        <div class="container">
            <div class="row py-4">
                <div class="col-md-4 py-3">
                    <div class="row fh5co_contact_us_no_icon_difh5co_hover">
                        <div class="col-3 fh5co_contact_us_no_icon_difh5co_hover_1">
                            <div class="fh5co_contact_us_no_icon_div"> <span><i class="fa fa-phone"></i></span> </div>
                        </div>
                        <div class="col-9 align-self-center fh5co_contact_us_no_icon_difh5co_hover_2">
                            <span class="c_g d-block">{{ __("words.pages.contact-us.call") }}</span>
                            <span class="d-block c_g fh5co_contact_us_no_text">{{ $contacts->phone_number }}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4 py-3">
                    <div class="row fh5co_contact_us_no_icon_difh5co_hover">
                        <div class="col-3 fh5co_contact_us_no_icon_difh5co_hover_1">
                            <div class="fh5co_contact_us_no_icon_div"> <span><i class="fa fa-envelope"></i></span> </div>
                        </div>
                        <div class="col-9 align-self-center fh5co_contact_us_no_icon_difh5co_hover_2">
                            <span class="c_g d-block">{{ __("words.pages.contact-us.questions") }}?</span>
                            <span class="d-block c_g fh5co_contact_us_no_text">{{ $contacts->email }}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4 py-3">
                    <div class="row fh5co_contact_us_no_icon_difh5co_hover">
                        <div class="col-3 fh5co_contact_us_no_icon_difh5co_hover_1">
                            <div class="fh5co_contact_us_no_icon_div"> <span><i class="fa fa-map-marker"></i></span> </div>
                        </div>
                        <div class="col-9 align-self-center fh5co_contact_us_no_icon_difh5co_hover_2">
                            <span class="c_g d-block">{{ __("words.pages.contact-us.address") }}</span>
                            <span class="d-block c_g fh5co_contact_us_no_text">{{ $contacts->address }}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--------------------------------------------- Contact  -------------------------------------------->

    <!---------------------------------------------- News  ---------------------------------------------->
    <div class="container-fluid mb-4">
        <div class="container py-5">
            <iframe src="{{ $contacts->maps_embed }}" class="map_sss" style="border:0; border-radius: 17px; width: 100%; height: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </div>
    <!---------------------------------------------- News  ---------------------------------------------->
@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush