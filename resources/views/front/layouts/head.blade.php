<style>
    .btn-rounded-31 {
        border-radius: 31px;
        margin-right: 7px;
    }
</style>
<div class="container-fluid fh5co_header_bg">
    <div class="container">
        <div class="row">
            <div class="col-12 fh5co_mediya_center">
                <a href="#" class="color_fff fh5co_mediya_setting">
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;{{ date('l, d F Y') }}
                </a>
                <div class="d-inline-block fh5co_trading_posotion_relative"><a href="{{ request()->is(app()->getLocale().'/home', app()->getLocale().'/home/*') ? '#trending' : route('news', app()->getLocale()) }}" class="treding_btn">{{ __("words.layouts.trending") }}</a>
                    <div class=""></div>
                </div>
                <a href="#" class="color_fff fh5co_mediya_setting">{{ pageSetup()->headline }}</a>
            </div>
        </div>
    </div>
</div>
    
<div class="container-fluid bg-faded fh5co_padd_mediya padding_786">
    <div class="container py-3" style="padding: 0px;">
        <nav class="navbar">
            <div class="row">
                <div class="col-md-3 col-3">
                    <button class="navbar-toggler" style="border: 2px solid #3a3a3a; padding: 7px 12px 7px 12px;" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span>
                    </button>
                </div>
                <div class="col-md-9 col-9 text-right">
                    <a href="{{ url('/') }}"><img src="{{ url(pageSetup()->logo_image) }}" style="height: 31px; margin-top: 4px; width: auto;" alt="{{ pageSetup()->logo_alt }}"/></a>
                </div>
            </div>
        </nav>

        <div class="collapse px-3 pb-2" id="navbarToggleExternalContent">
            <a href="{{ route('news', app()->getLocale()) }}" class="btn mt-3 {{ request()->is(app()->getLocale().'/news', app()->getLocale().'/news/*') ? 'btn-primary' : 'btn-outline-primary' }} btn-rounded-31">News</a>
            <a href="{{ route('events', app()->getLocale()) }}" class="btn mt-3 {{ request()->is(app()->getLocale().'/events', app()->getLocale().'/events/*') ? 'btn-primary' : 'btn-outline-primary' }} btn-rounded-31">Events</a>
            <a href="{{ route('about-us', app()->getLocale()) }}" class="btn mt-3 {{ request()->is(app()->getLocale().'/about-us', app()->getLocale().'/about-us/*') ? 'btn-primary' : 'btn-outline-primary' }} btn-rounded-31">About</a>
            <a href="{{ route('contact', app()->getLocale()) }}" class="btn mt-3 {{ request()->is(app()->getLocale().'/contact', app()->getLocale().'/contact/*') ? 'btn-primary' : 'btn-outline-primary' }} btn-rounded-31">Contact Us</a>

            <span class="dropdown p-0 text-center">
                <a href="#" class="btn dropdown-toggle mt-3" style="border-radius: 31px; color: #ffffff; background: #3a3a3a;" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-language"></i> &nbsp; {{ app()->getLocale() === 'id' ? 'Indonesia' : 'English' }} <span class="sr-only">(current)</span>
                </a>
                
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                    <a class="dropdown-item {{ app()->getLocale() === 'id' ? 'active' : '' }}" href="{{ request()->is(app()->getLocale()) ? Str::replace('/en', '/id', request()->url()) : Str::replace('/en/', '/id/', request()->url()) }}">Indonesia</a>
                    <a class="dropdown-item {{ app()->getLocale() === 'en' ? 'active' : '' }}" href="{{ request()->is(app()->getLocale()) ? Str::replace('/id', '/en', request()->url()) : Str::replace('/id/', '/en/', request()->url()) }}">English</a>
                </div>
            </span>
        </div>
        
    </div>
</div>