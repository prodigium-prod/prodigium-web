<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <!-- Required meta tags -->    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{ env('APP_URL')}}">

    <title>@yield('title', config('app.name'))</title>
    
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    
    <meta property="og:image" content="{{ url(pageSetup()->meta_icon) }}">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <meta property="og:type" content="website" />
	<meta property="og:title" content="@yield('title', config('app.name'))" />
    <meta property="og:url" content="@yield('url', env('APP_URL'))" />
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="og:description" content="@yield('descriptions', pageSetup()->meta_descriptions)" />
    <!-- <meta property="twitter:image" content="{{ pageSetup()->meta_icon }}">
    <meta property="twitter:card" content="website">
    <meta property="twitter:url" content="{{ url('/') }}">
    <meta property="twitter:title" content="{{ config('app.name', config('app.name')) }}">
    <meta property="twitter:description" content="PRODIGY Descriptions"> -->
    
    <meta name="robots" content="index, follow, max-snippet:120">
    <meta name="description" content="@yield('descriptions', pageSetup()->meta_descriptions)">
    <meta name="keywords" content="@yield('keywords', pageSetup()->meta_keywords)">

    <link rel="icon" href="{{ url(pageSetup()->meta_icon) }}" type="image/gif" sizes="16x16">

    <link href="{{ url('assets/front/css/media_query.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/front/css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/front/css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/front/css/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/front/css/owl.theme.default.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap CSS -->
    <link href="{{ url('assets/front/css/style_1.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Modernizr JS -->
    <script src="{{ url('assets/front/js/modernizr-3.5.0.min.js') }}"></script>

    <style>
        .vertical-center {
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }
        
        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px #7c7c7c; 
            border-radius: 10px;
        }
        
        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #2c2a3400; 
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #0d6efd; 
        }
    </style>

    <!---------------- Custom Style  ---------------->
        @stack('styles')
    <!---------------- Custom Style  ---------------->
</head>
<body class="single">
    <!-------------------- Head  -------------------->
        @include('front.layouts.head')
    <!-------------------- Head  -------------------->

    <!-------------------- Body  -------------------->
        @yield('content')
    <!-------------------- Body  -------------------->

    <!------------------- Footer  ------------------->
        @include('front.layouts.footer')
    <!------------------- Footer  ------------------->

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="fa fa-arrow-up"></i></a>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ url('assets/front/js/owl.carousel.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <!-- Waypoints -->
    <script src="{{ url('assets/front/js/jquery.waypoints.min.js') }}"></script>
    <!-- Parallax -->
    <script src="{{ url('assets/front/js/jquery.stellar.min.js') }}"></script>
    <!-- Main -->
    <script src="{{ url('assets/front/js/main.js') }}"></script>

    <!---------------- Custom Script  ---------------->
        @stack('scripts')
    <!---------------- Custom Script  ---------------->
</body>
</html>