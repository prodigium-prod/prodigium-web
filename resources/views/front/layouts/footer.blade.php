<div class="container-fluid fh5co_footer_bg pb-3">
    <div class="container animate-box">
        <div class="row">
            <div class="col-12 col-md-12 mt-5 text-center">
                <div class="footer_mediya_icon">
                    <div class="text-center d-inline-block">
                        <a href="{{ fetchContact()->linkedin }}" class="fh5co_display_table_footer">
                            <div class="fh5co_verticle_middle">
                                <i class="fa fa-linkedin"></i>
                            </div>
                        </a>
                    </div>
                    <div class="text-center d-inline-block">
                        <a href="{{ fetchContact()->google }}" class="fh5co_display_table_footer">
                            <div class="fh5co_verticle_middle">
                                <i class="fa fa-google-plus"></i>
                            </div>
                        </a>
                    </div>
                    <div class="text-center d-inline-block">
                        <a href="{{ fetchContact()->twitter }}" class="fh5co_display_table_footer">
                            <div class="fh5co_verticle_middle">
                                <i class="fa fa-twitter"></i>
                            </div>
                        </a>
                    </div>
                    <div class="text-center d-inline-block">
                        <a href="{{ fetchContact()->instagram }}" class="fh5co_display_table_footer">
                            <div class="fh5co_verticle_middle">
                                <i class="fa fa-instagram"></i>
                            </div>
                        </a>
                    </div>
                    <div class="text-center d-inline-block">
                        <a href="{{ fetchContact()->facebook }}" class="fh5co_display_table_footer">
                            <div class="fh5co_verticle_middle">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </a>
                    </div>
                    <div class="text-center d-inline-block">
                        <a href="{{ fetchContact()->youtube }}" class="fh5co_display_table_footer">
                            <div class="fh5co_verticle_middle">
                                <i class="fa fa-youtube-play"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5 pt-2 pb-4">
            <div class="col-12 col-md-8 col-lg-7 ">
                <form action="{{ route('subscribe.now', app()->getLocale()) }}" method="POST">
                    @csrf
                    <div class="input-group">
                        <span class="input-group-addon fh5co_footer_text_box" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                        <input type="email" name="email" class="form-control fh5co_footer_text_box" placeholder="{{ __('words.footer.subscribe.placeholder') }}..." aria-describedby="basic-addon1" required>
                        <button type="submit" class="input-group-addon fh5co_footer_subcribe" id="basic-addon12"> <i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;{{ __("words.footer.subscribe.button") }}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid fh5co_footer_right_reserved">
    <div class="container">
        <div class="row  ">
            <div class="col-12 col-md-6 py-4 Reserved"> © Copyright 2023, All rights reserved.</div>
            <div class="col-12 col-md-6 spdp_right py-4">
                <a href="{{ route('about-us', app()->getLocale()) }}" class="footer_last_part_menu">{{ __("words.footer.about-us") }}</a>
                <a href="{{ route('contact', app()->getLocale()) }}" class="footer_last_part_menu">{{ __("words.footer.contact") }}</a>
                <a href="{{ route('faq', app()->getLocale()) }}" class="footer_last_part_menu">{{ __("words.footer.faq") }}</a>
            </div>
        </div>
    </div>
</div>