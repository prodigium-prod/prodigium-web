@if ($paginator->hasPages())
    <div class="col-12 text-center pb-4 pt-4">
        @if ($paginator->onFirstPage())
            <a href="javascript:;" class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;</a>
        @endif

        @foreach ($elements as $element)
        
            @if (is_string($element))
                <a href="javascript:;" class="btn_pagging_active">{{ $element }}</a>
            @endif
        
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="javascript:;" class="btn_pagging_active">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach
        
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="btn_mange_pagging"><i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp; </a>
        @else
            <a href="javascript:;" class="btn_mange_pagging"><i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp; </a>
        @endif
    </div>
@else
    <div class="col-12 text-center pb-4 pt-4">
        <a href="javascript:;" class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;</a>
        <a href="javascript:;" class="btn_pagging_active">1</a>
        <a href="javascript:;" class="btn_mange_pagging"><i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp; </a>
    </div>
@endif