@extends("front.layouts.master")

<!-- Set Title -->
@section('title', config('app.name')." | ".$news->title(app()->getLocale()) ? $news->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...')
@section('url', Request::url())
@section('descriptions', $news->description(app()->getLocale()) ? $news->description(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...')
@section('keywords', $news->meta_keyword ? $news->meta_keyword : 'prodigium')

@push('styles')
    <!-- Add custom styles  -->
@endpush
@section("content")
    <!---------------------------------------------- News Details  ---------------------------------------------->
    <div id="fh5co-title-box" style="background-image: url({{ isset($news->banner) ? url($news->banner) : url('assets/images/banner_960.jpg') }}); background-position: 10% 0px;" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="page-title">
            <a href="{{ isset($news->banner) ? url($news->banner) : url('assets/images/banner_960.jpg') }}" target="_blank" rel="noopener noreferrer">
                <img src="{{ url('assets/images/zoom-banner.png') }}" alt="Prodigium" style="margin-left: -17px;">
            </a>
            <span>{{ isset($news->create_at) ? date('F d, Y', strtotime($news->create_at)) : date('F d, Y') }}</span>
            <span style="font-size: 22px; font-weight: 800; color: #fff;">{{ $news->title(app()->getLocale()) ? $news->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...' }}</span>
        </div>
    </div>

    <div id="fh5co-single-content" class="container-fluid pb-4 pt-4 paddding">
        <div class="container paddding">
            <div class="row mx-0">
                <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft" style="">
                    {!! $news->content(app()->getLocale()) !!}
                </div>
                
                <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                    <div>
                        <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">{{ __("words.landingpage.tags") }}</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="fh5co_tags_all">
                        @foreach($news->tags() as $valueTag)
                            <a href="#" class="fh5co_tagg">{{ $valueTag }}</a>
                        @endforeach
                    </div>
                    <div>
                        <div class="fh5co_heading fh5co_heading_border_bottom pt-3 py-2 mb-4">{{ __("words.landingpage.most-popular") }}</div>
                    </div>

                    @foreach($mostPopular as $mPopular)
                    <div class="row pb-3">
                        <div class="col-5 align-self-center">
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $mPopular->news_code, 'slug' => $mPopular->slug(app()->getLocale()) ? $mPopular->slug(app()->getLocale()) : '-']) }}">
                                <img src="{{ isset($mPopular->banner) ? url($mPopular->banner) : url('assets/images/banner_960.jpg') }}" alt="img" class="fh5co_most_trading"/>
                            </a>
                        </div>
                        <div class="col-7 paddding">
                            <div class="most_fh5co_treding_font"> 
                                
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $mPopular->news_code, 'slug' => $mPopular->slug(app()->getLocale()) ? $mPopular->slug(app()->getLocale()) : '-']) }}">
                                {{ mb_strimwidth($mPopular->title(app()->getLocale()) ? $mPopular->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }}
                            </a></div>
                            <div class="most_fh5co_treding_font_123"> {{ isset($mPopular->create_at) ? date('F d, Y', strtotime($mPopular->create_at)) : date('F d, Y') }}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!---------------------------------------------- News Details  ---------------------------------------------->
@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush