@extends("front.layouts.master")

<!-- Set Title -->
@section('title')

@push('styles')
    <!-- Add custom styles  -->
@endpush
@section("content")
    <!--------------------------------------------- Banner  --------------------------------------------->
    <div class="container-fluid paddding mb-5">
        <div class="row mx-0">
            <div class="col-md-6 col-12 paddding animate-box" data-animate-effect="fadeIn">
                <div class="fh5co_suceefh5co_height"><img src="{{ isset($mainBanner->banner) ? url($mainBanner->banner) : url('assets/images/banner_960.jpg') }}" alt="img"/>
                    <div class="fh5co_suceefh5co_height_position_absolute"></div>
                    <div class="fh5co_suceefh5co_height_position_absolute_font">
                        <div class="">
                            <a href="#" class="color_fff"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ isset($mainBanner->create_at) ? date('F d, Y', strtotime($mainBanner->create_at)) : date('F d, Y') }}</a>
                        </div>
                        <div class=""><a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => isset($mainBanner->news_code) ? $mainBanner->news_code : '0', 'slug' => $mainBannerSlug ? $mainBannerSlug : '-']) }}" class="fh5co_good_font"> {{ mb_strimwidth($mainBannerTitle ? $mainBannerTitle : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }} </a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    @foreach($otherBanner as $keyBanner => $valueBanner)
                    <div class="col-md-6 col-6 paddding animate-box" data-animate-effect="fadeIn">
                        <div class="fh5co_suceefh5co_height_2"><img src="{{ isset($valueBanner->banner) ? url($valueBanner->banner) : url('assets/images/banner_960.jpg') }}" alt="img"/>
                            <div class="fh5co_suceefh5co_height_position_absolute"></div>
                            <div class="fh5co_suceefh5co_height_position_absolute_font_2">
                                <div class="">
                                    <a href="#" class="color_fff"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ isset($valueBanner->create_at) ? date('F d, Y', strtotime($valueBanner->create_at)) : date('F d, Y') }}</a>
                                </div>
                                <div class=""><a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $valueBanner->news_code, 'slug' => $valueBanner->slug(app()->getLocale()) ? $valueBanner->slug(app()->getLocale()) : '-']) }}" class="fh5co_good_font_2">{{ mb_strimwidth($valueBanner->title(app()->getLocale()) ? $valueBanner->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }} </a></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!--------------------------------------------- Banner  --------------------------------------------->

    <!-------------------------------------------- Trending  -------------------------------------------->
    <div class="container-fluid pt-3" id="trending">
        <div class="container animate-box" data-animate-effect="fadeIn">
            <div>
                <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">{{ __("words.landingpage.trending") }}</div>
            </div>
            <div class="owl-carousel owl-theme js" id="slider1">
                @foreach(fetchNews(6) as $fetchNewsX)
                    <div class="item">
                        <div class="fh5co_latest_trading_img_position_relative">
                            <div class="fh5co_latest_trading_img"><img src="{{ isset($fetchNewsX->banner) ? url($fetchNewsX->banner) : url('assets/images/banner_960.jpg') }}" alt="" class="fh5co_img_special_relative"/></div>
                            <div class="fh5co_latest_trading_img_position_absolute"></div>
                            <div class="fh5co_latest_trading_img_position_absolute_1">
                                <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}"" class="text-white"> {{ mb_strimwidth($fetchNewsX->title(app()->getLocale()) ? $fetchNewsX->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }} </a>
                                <div class="fh5co_latest_trading_date_and_name_color"> {{ isset($fetchNewsX->create_at) ? date('F d, Y', strtotime($fetchNewsX->create_at)) : date('F d, Y') }}</div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <!-- <div class="item">
                    <div class="fh5co_latest_trading_img_position_relative">
                        <div class="fh5co_latest_trading_img"></div>
                        <div class="fh5co_latest_trading_img_position_absolute">
                            <div class="vertical-center w-100 text-center">
                                <button class="btn btn-primary" style="border-radius: 31px;">Show More</button>
                            </div>
                        </div>
                        <div class="fh5co_latest_trading_img_position_absolute_1"></div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-------------------------------------------- Trending  -------------------------------------------->

    <!--------------------------------------------- Events  --------------------------------------------->
    <div class="container-fluid pb-4 pt-5">
        <div class="container animate-box">
            <div>
                <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">{{ __("words.landingpage.events") }}</div>
            </div>
            <div class="owl-carousel owl-theme" id="slider2">
                @foreach($events as $s_event)
                <div class="item">
                    <div class="fh5co_hover_news_img">
                        <div class="fh5co_news_img"><img src="{{ isset($s_event->banner) ? url($s_event->banner) : url('assets/images/banner_960.jpg') }}" alt=""/></div>
                        <div>
                            <a href="{{ route('events-detail', ['lang' => app()->getLocale(), 'code' => $s_event->event_code, 'slug' => $s_event->slug(app()->getLocale()) ? $s_event->slug(app()->getLocale()) : '-']) }}" class="d-block fh5co_small_post_heading"><span class="">Lorem Ipsum is simply dummy text...</span></a>
                            <div class="c_g"><i class="fa fa-clock-o"></i> January 23, 2023</div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!--------------------------------------------- Events  --------------------------------------------->

    <!---------------------------------------------- News  ---------------------------------------------->
    <div class="container-fluid pb-4 pt-4 paddding">
        <div class="container paddding">
            <div class="row mx-0">
                <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                    <div>
                        <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">{{ __("words.landingpage.news") }}</div>
                    </div>
                    
                    @foreach($news as $fetchNewsX)
                    <div class="row pb-4">
                        <div class="col-md-5">
                            <div class="fh5co_hover_news_img">
                                <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}">
                                    <div class="fh5co_news_img">
                                        <img src="{{ isset($fetchNewsX->banner) ? url($fetchNewsX->banner) : url('assets/images/banner_960.jpg') }}" alt=""/>
                                    </div>
                                </a>
                                <div></div>
                            </div>
                        </div>
                        <div class="col-md-7 animate-box">
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}" class="fh5co_magna py-2"> 
                                {{ mb_strimwidth($fetchNewsX->title(app()->getLocale()) ? $fetchNewsX->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }}
                            </a> <br/>
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}" class="fh5co_mini_time py-3"> {{ isset($fetchNewsX->create_at) ? date('F d, Y', strtotime($fetchNewsX->create_at)) : date('F d, Y') }} </a>
                            <div class="fh5co_consectetur"> 
                                {{ $fetchNewsX->description(app()->getLocale()) ? $fetchNewsX->description(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...' }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                    <div>
                        <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">{{ __("words.landingpage.tags") }}</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="fh5co_tags_all">
                        @foreach($tags as $s_tag)
                            <a href="#" class="fh5co_tagg">{{ $s_tag->tag }}</a>
                        @endforeach
                    </div>
                    
                    <div class="mt-4">
                        <div class="fh5co_heading fh5co_heading_border_bottom pt-3 py-2 mb-4">{{ __("words.landingpage.most-popular") }}</div>
                    </div>
                    @foreach(fetchNews(4) as $fetchNewsX)
                        <div class="row pb-3">
                            <div class="col-5 align-self-center">
                                <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}">
                                    <img src="{{ isset($fetchNewsX->banner) ? url($fetchNewsX->banner) : url('assets/images/banner_960.jpg') }}" alt="img" class="fh5co_most_trading"/>
                                </a>
                            </div>
                            <div class="col-7 paddding">
                                <div class="most_fh5co_treding_font"> 
                                    
                                <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}">
                                    {{ mb_strimwidth($fetchNewsX->title(app()->getLocale()) ? $fetchNewsX->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }}
                                </a></div>
                                <div class="most_fh5co_treding_font_123"> {{ isset($fetchNewsX->create_at) ? date('F d, Y', strtotime($fetchNewsX->create_at)) : date('F d, Y') }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row mx-0 animate-box" data-animate-effect="fadeInUp">
                <div class="col-12 text-center pb-4 pt-4">
                    <a href="{{ route('news', app()->getLocale()) }}" class="btn_mange_pagging px-5" style="border-radius: 31px;">{{ __("words.landingpage.show-more") }}</a>
                </div>
            </div>
        </div>
    </div>
    <!---------------------------------------------- News  ---------------------------------------------->
@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush