@extends("front.layouts.master")

<!-- Set Title -->
@section('title', config('app.name')." | ".__("words.pages.about-us.title"))

@push('styles')
    <!-- Add custom styles  -->
    <style>
        @media only screen and (max-width: 600px) {
            .desktop-display {
                display: none;
            }
        }
    </style>
@endpush
@section("content")
    <!-------------------------------------------- About Us  -------------------------------------------->
    <div class="container-fluid contact_us_bg_img">
        <div class="container">
            <div class="text-center">
                <a href="#" class="fh5co_con"> {{ __("words.pages.about-us.title") }} </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container pt-2">

            <div class="fh5co_news_img desktop-display mt-4" style="height:317px;">
                <img src="{{ $banner }}" alt=""/>
            </div>
            <div class="pt-4">
                {!! $content !!}
            </div>
        </div>
    </div>
    <!-------------------------------------------- About Us  -------------------------------------------->
    
    <!--------------------------------------------- Events  --------------------------------------------->
    <div class="container-fluid mt-5 pb-5 mb-5">
        <div class="container animate-box">
            <div>
                <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">{{ __("words.pages.about-us.events") }}</div>
            </div>
            <div class="owl-carousel owl-theme" id="slider2">
                <div class="item">
                    <div class="fh5co_hover_news_img">
                        <div class="fh5co_news_img"><img src="{{ url('assets/images/banner_960.jpg') }}" alt=""/></div>
                        <div>
                            <a href="javascript:;" class="d-block fh5co_small_post_heading"><span class="">Lorem Ipsum is simply dummy text...</span></a>
                            <div class="c_g"><i class="fa fa-clock-o"></i> January 23, 2023</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="fh5co_hover_news_img">
                        <div class="fh5co_news_img"><img src="{{ url('assets/images/banner_960.jpg') }}" alt=""/></div>
                        <div>
                            <a href="javascript:;" class="d-block fh5co_small_post_heading"><span class="">Lorem Ipsum is simply dummy text...</span></a>
                            <div class="c_g"><i class="fa fa-clock-o"></i> January 23, 2023</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="fh5co_hover_news_img">
                        <div class="fh5co_news_img"><img src="{{ url('assets/images/banner_960.jpg') }}" alt=""/></div>
                        <div>
                            <a href="javascript:;" class="d-block fh5co_small_post_heading"><span class="">Lorem Ipsum is simply dummy text...</span></a>
                            <div class="c_g"><i class="fa fa-clock-o"></i> January 23, 2023</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="fh5co_hover_news_img">
                        <div class="fh5co_news_img"><img src="{{ url('assets/images/banner_960.jpg') }}" alt=""/></div>
                        <div>
                            <a href="javascript:;" class="d-block fh5co_small_post_heading"><span class="">Lorem Ipsum is simply dummy text...</span></a>
                            <div class="c_g"><i class="fa fa-clock-o"></i> January 23, 2023</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="fh5co_hover_news_img">
                        <div class="fh5co_news_img" style="background: #37393c;">
                            <div class="vertical-center w-100 text-center">
                                <button class="btn btn-primary" style="border-radius: 31px;">Show More</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------------------------------------------- Events  --------------------------------------------->
@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush