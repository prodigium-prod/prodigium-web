<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->    
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="app-url" content="{{ env('APP_URL')}}">

        <title>@yield('title', config('app.name')) | Product & Services</title>
        
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        
        <meta property="og:image" content="{{ url(pageSetup()->meta_icon) }}">
        <meta property="og:image:width" content="200">
        <meta property="og:image:height" content="200">
        <meta property="og:type" content="website" />
        <meta property="og:title" content="@yield('title', config('app.name'))" />
        <meta property="og:url" content="{{ env('APP_URL')}}" />
        <meta property="og:site_name" content="{{ config('app.name') }}" />
        <meta property="og:description" content="{{ pageSetup()->meta_descriptions }}" />
        
        <meta name="robots" content="index, follow, max-snippet:120">
        <meta name="description" content="{{ pageSetup()->meta_descriptions }}">
        <meta name="keywords" content="{{ pageSetup()->meta_keywords }}">

        <link rel="icon" href="{{ url(pageSetup()->meta_icon) }}" type="image/gif" sizes="16x16">

        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ url('assets/main/css/styles.css') }}" rel="stylesheet" />
        <style>
            html {
                /* height: 100vh; */
            }
            body {
                /* min-height: 100vh; */
                background: #323232;
            }

            /* width */
            ::-webkit-scrollbar {
                width: 0px;
            }

            /* Track */
            ::-webkit-scrollbar-track {
                /* box-shadow: inset 0 0 5px #2c2a3400; 
                border-radius: 10px; */
            }
            
            /* Handle */
            ::-webkit-scrollbar-thumb {
                /* background: #2c2a3400; 
                border-radius: 10px; */
            }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
                /* background: #0d6efd;  */
            }

            .container-center {
                position: relative;
                height: 100%;
            }

            video {
                object-fit: cover;
                width: 100vw;
                height: 100vh;
                position: fixed;
                top: 0;
                left: 0;
                z-index: -117;
            }

            .centered-element-center {
                margin: 0;
                width: 100%;
                position: absolute;
                top: 48%;
                transform: translateY(-50%);
            }
            .logo-cust-setup {
                height: 27px; width: auto;
            }
            
            @media only screen and (max-width: 821px) {
                .logo-cust-setup {
                    margin-right: -7px;
                }
            }
            .container_x {
                display: inline-block;
                cursor: pointer;
            }

            .bar1, .bar2, .bar3 {
                width: 35px;
                height: 5px;
                background-color: #fff;
                margin: 6px 0;
                transition: 0.4s;
            }

            .change .bar1 {
                transform: translate(0, 11px) rotate(-45deg);
            }

            .change .bar2 {opacity: 0;}

            .change .bar3 {
                transform: translate(0, -11px) rotate(45deg);
            }
        </style>
    </head>
    <body class="sb-sidenav-toggled">
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="" style="background: #9771ae58; z-index: 117;" id="sidebar-wrapper">
                <!-- <div class="sidebar-heading text-white" style="background: #202020; font-weight: 900; font-size: 22px;">PRODIGIUM.ID</div> -->
                <div class="list-group list-group-flush">
                    <a href="{{ url('/') }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700; {{ request()->is('/') ? 'color: #9771ae;' : '' }}">Home</a>
                    <a href="{{ route('productServices', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700; {{ request()->is(app()->getLocale().'/product-services', app()->getLocale().'/product-services/*') ? 'color: #9771ae;' : '' }}">Product & Services</a>
                    <a href="{{ route('news', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700;">News</a>
                    <a href="{{ route('contact', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700;">Contact</a>
                    <a href="{{ route('about-us', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700;">About</a>
                    <!-- <a href="{{ route('events', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700;">Events</a> -->
                    <!-- <a href="{{ route('faq', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4">FAQ</a> -->
                    <!-- <a href="{{ route('contact', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700;">Get In Touch</a> -->
                    <!-- <a href="{{ route('contact', app()->getLocale()) }}" class="list-group-item list-group-item-action p-3 ps-4" style="text-align: right; font-size: 22px; font-weight: 700;">Partnership</a> -->
                </div>
            </div>
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-fixed-top navbar-light" style="background: #c5c5c500; max-height: 10vh;">
                    <div class="container-fluid">
                        <div class="container_x" id="sidebarToggle" onclick="myFunction(this)">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                        <!-- <a id="sidebarToggle" style="margin: 7px;"><i class="fa fa-bars text-white" style="font-size: 31px;"></i></a> -->
                        <!-- <span style="font-weight: 900; font-size: 31px; color: #fff;">PRODIGIUM</span> -->
                        <img src="{{ url('assets/images/PRODIGIUM.png') }}" alt="{{ pageSetup()->logo_alt }}" class="logo-cust-setup"/>
                    </div>
                </nav>
                <!-- Page content-->
                <div class="w-100 bg-warning" style="max-height: 90vh; overflow: auto">
                    <video src="{{ url('assets/videos/BannerProductServices.mp4') }}" autoplay loop playsinline muted></video>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ url('assets/main/js/scripts.js') }}"></script>
        <script>
            function myFunction(x) {
                x.classList.toggle("change");
            }
        </script>
    </body>
</html>
