@extends("front.layouts.master")

<!-- Set Title -->
@section('title', config('app.name')." | Events")

@push('styles')
    <!-- Add custom styles  -->
@endpush
@section("content")
    <!---------------------------------------------- Events  ---------------------------------------------->
    <div class="container-fluid contact_us_bg_img">
        <div class="container">
            <div class="text-center">
                <a href="#" class="fh5co_con"> {{ __("words.pages.event.title") }} </a>
            </div>
        </div>
    </div>
    
    <div class="container-fluid pb-4 pt-4 paddding">
        <div class="container paddding">
            <div class="px-3">
                <form action="">
                    <div class="input-group mb-3">
                        <input type="text" name="q" class="form-control" value="{{ Request::has('q') ? Request::get('q') : '' }}" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-outline-primary" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    @if(Request::has('q'))
                    <div class="text-center pb-3">
                        <a href="{{ route('events', app()->getLocale()) }}" class="btn btn-primary">Show All</a>
                    </div>
                    @endif
                </form>
            </div>
            <div class="row mx-0">
                <div class="col-md-8 animate-box pt-3" data-animate-effect="fadeInLeft">
                    @if(count($events) > 0)
                        @foreach($events as $fetchEventsX)
                        <div class="row pb-4">
                            <div class="col-md-5">
                                <a href="{{ route('events-detail', ['lang' => app()->getLocale(), 'code' => $fetchEventsX->event_code, 'slug' => $fetchEventsX->slug(app()->getLocale()) ? $fetchEventsX->slug(app()->getLocale()) : '-']) }}">
                                    <div class="fh5co_hover_news_img">
                                        <div class="fh5co_news_img">
                                            <img src="{{ isset($fetchEventsX->banner) ? url($fetchEventsX->banner) : url('assets/images/banner_960.jpg') }}" alt=""/>
                                        </div>
                                        <div></div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-7 animate-box">
                                <a href="{{ route('events-detail', ['lang' => app()->getLocale(), 'code' => $fetchEventsX->event_code, 'slug' => $fetchEventsX->slug(app()->getLocale()) ? $fetchEventsX->slug(app()->getLocale()) : '-']) }}" class="fh5co_magna py-2"> 
                                    {{ mb_strimwidth($fetchEventsX->title(app()->getLocale()) ? $fetchEventsX->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }}
                                </a> 
                                <a href="{{ route('events-detail', ['lang' => app()->getLocale(), 'code' => $fetchEventsX->event_code, 'slug' => $fetchEventsX->slug(app()->getLocale()) ? $fetchEventsX->slug(app()->getLocale()) : '-']) }}" class="fh5co_mini_time py-3"></a>
                                <div class="py-3">
                                    <span>{{ isset($fetchEventsX->create_at) ? date('F d, Y', strtotime($fetchEventsX->create_at)) : date('F d, Y') }}</span>
                                </div>
                                <div class="fh5co_consectetur"> 
                                    {{ $fetchEventsX->description(app()->getLocale()) ? $fetchEventsX->description(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...' }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="text-start">
                            <strong style="font-size: 31px; color: #9d9d9d;">Not Found...</strong>
                        </div>
                    @endif
                </div>
                <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                    <div>
                        <div class="fh5co_heading fh5co_heading_border_bottom pt-3 py-2 mb-4">{{ __("words.landingpage.news") }}</div>
                    </div>
                    @foreach(fetchNews(4) as $fetchEventsX)
                        <div class="row pb-3">
                            <div class="col-5 align-self-center">
                                <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchEventsX->news_code, 'slug' => $fetchEventsX->slug(app()->getLocale()) ? $fetchEventsX->slug(app()->getLocale()) : '-']) }}">
                                    <img src="{{ isset($fetchEventsX->banner) ? url($fetchEventsX->banner) : url('assets/images/banner_960.jpg') }}" alt="img" class="fh5co_most_trading"/>
                                </a>
                            </div>
                            <div class="col-7 paddding">
                                <div class="most_fh5co_treding_font"> 
                                    
                                <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchEventsX->news_code, 'slug' => $fetchEventsX->slug(app()->getLocale()) ? $fetchEventsX->slug(app()->getLocale()) : '-']) }}">
                                    {{ mb_strimwidth($fetchEventsX->title(app()->getLocale()) ? $fetchEventsX->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }}
                                </a></div>
                                <div class="most_fh5co_treding_font_123"> {{ isset($fetchEventsX->create_at) ? date('F d, Y', strtotime($fetchEventsX->create_at)) : date('F d, Y') }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row mx-0 animate-box" data-animate-effect="fadeInUp">
                {{ $events->links('front.layouts.pagination') }}
            </div>
        </div>
    </div>
    <!---------------------------------------------- Events  ---------------------------------------------->
@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush