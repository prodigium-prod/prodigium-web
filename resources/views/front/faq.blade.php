@extends("front.layouts.master")

<!-- Set Title -->
@section('title', config('app.name')." | ".__("words.pages.faq.title"))

@push('styles')
    <!-- Add custom styles  -->
    <style>
        @media only screen and (max-width: 600px) {
            .desktop-display {
                display: none;
            }
        }
    </style>
@endpush
@section("content")
    <!-------------------------------------------- FAQ  -------------------------------------------->
    <div class="container-fluid contact_us_bg_img">
        <div class="container">
            <div class="text-center">
                <a href="#" class="fh5co_con"> {{ __("words.pages.faq.title") }} </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container">
            <div class="py-5">
                <div id="accordion">
                    @foreach($faq as $key => $value)
                    <div class="card">
                        <a class="card-link" data-toggle="collapse" href="#collapse{{ $value->id }}">
                            <div class="card-header" style="font-size: 17px; font-weight: 800;">
                                {{ app()->getLocale() === 'id' ? $value->title_id : $value->title_en }}
                            </div>
                        </a>
                        <div id="collapse{{ $value->id }}" class="collapse {{ $key == 0 ? 'show' : '' }}" data-parent="#accordion">
                            <div class="card-body" style="padding: 20px;">
                                {{ app()->getLocale() === 'id' ? $value->description_id : $value->description_en }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-------------------------------------------- FAQ  -------------------------------------------->

@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush