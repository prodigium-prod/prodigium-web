@extends("front.layouts.master")

<!-- Set Title -->
@section('title', config('app.name')." | ".__("words.pages.news.title"))

@push('styles')
    <!-- Add custom styles  -->
@endpush
@section("content")
    <!--------------------------------------------- Banner  --------------------------------------------->
    <div class="container-fluid paddding mb-5" style="background: #222222;">
        <div class="row mx-0">
            <div class="col-md-6 col-12 paddding animate-box" data-animate-effect="fadeIn">
                <div class="fh5co_suceefh5co_height"><img src="{{ isset($mainBanner->banner) ? url($mainBanner->banner) : url('assets/images/banner_960.jpg') }}" alt="img"/>
                    <div class="fh5co_suceefh5co_height_position_absolute"></div>
                    <div class="fh5co_suceefh5co_height_position_absolute_font">
                        <div class="">
                            <a href="#" class="color_fff"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ isset($mainBanner->create_at) ? date('F d, Y', strtotime($mainBanner->create_at)) : date('F d, Y') }}</a>
                        </div>
                        <div class=""><a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => isset($mainBanner->news_code) ? $mainBanner->news_code : '0', 'slug' => $mainBannerSlug ? $mainBannerSlug : '-']) }}" class="fh5co_good_font"> {{ mb_strimwidth($mainBannerTitle ? $mainBannerTitle : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }} </a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    @foreach($otherBanner as $keyBanner => $valueBanner)
                    <div class="col-md-6 col-6 paddding animate-box" data-animate-effect="fadeIn">
                        <div class="fh5co_suceefh5co_height_2"><img src="{{ isset($valueBanner->banner) ? url($valueBanner->banner) : url('assets/images/banner_960.jpg') }}" alt="img"/>
                            <div class="fh5co_suceefh5co_height_position_absolute"></div>
                            <div class="fh5co_suceefh5co_height_position_absolute_font_2">
                                <div class="">
                                    <a href="#" class="color_fff"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ isset($valueBanner->create_at) ? date('F d, Y', strtotime($valueBanner->create_at)) : date('F d, Y') }}</a>
                                </div>
                                <div class=""><a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $valueBanner->news_code, 'slug' => $valueBanner->slug(app()->getLocale()) ? $valueBanner->slug(app()->getLocale()) : '-']) }}" class="fh5co_good_font_2">{{ mb_strimwidth($valueBanner->title(app()->getLocale()) ? $valueBanner->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }} </a></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!--------------------------------------------- Banner  --------------------------------------------->
    
    <!---------------------------------------------- News  ---------------------------------------------->    
    <div class="container-fluid pb-4 paddding">
        <div class="container paddding">
            <div class="px-3">
                <form action="">
                    <div class="input-group mb-3">
                        <input type="text" name="q" class="form-control" value="{{ Request::has('q') ? Request::get('q') : '' }}" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-outline-primary" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    @if(Request::has('q'))
                    <div class="text-center pb-3">
                        <a href="{{ route('news', app()->getLocale()) }}" class="btn btn-primary">Show All</a>
                    </div>
                    @endif
                </form>
            </div>

            @if(count($news) > 0)
            <div class="row mx-0">
                <div class="col-md-12 animate-box pt-3" data-animate-effect="fadeInLeft">
                    @foreach($news as $fetchNewsX)
                    <div class="row pb-4">
                        <div class="col-md-5">
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}">
                                <div class="fh5co_hover_news_img">
                                    <div class="fh5co_news_img">
                                        <img src="{{ isset($fetchNewsX->banner) ? url($fetchNewsX->banner) : url('assets/images/banner_960.jpg') }}" alt=""/>
                                    </div>
                                    <div></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-7 animate-box">
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}" class="fh5co_magna py-2"> 
                                {{ mb_strimwidth($fetchNewsX->title(app()->getLocale()) ? $fetchNewsX->title(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...', 0, 40, "...") }}
                            </a> 
                            <a href="{{ route('news-detail', ['lang' => app()->getLocale(), 'code' => $fetchNewsX->news_code, 'slug' => $fetchNewsX->slug(app()->getLocale()) ? $fetchNewsX->slug(app()->getLocale()) : '-']) }}" class="fh5co_mini_time py-3"></a>
                            <div class="py-3">
                                <span>{{ isset($fetchNewsX->create_at) ? date('F d, Y', strtotime($fetchNewsX->create_at)) : date('F d, Y') }}</span>
                            </div>
                            <div class="fh5co_consectetur"> 
                                {{ $fetchNewsX->description(app()->getLocale()) ? $fetchNewsX->description(app()->getLocale()) : 'Lorem Ipsum is simply dummy text...' }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row mx-0 animate-box" data-animate-effect="fadeInUp">
                {{ $news->links('front.layouts.pagination') }}
            </div>
            @else
                <div class="text-center">
                    <strong style="font-size: 31px; color: #9d9d9d;">Not Found...</strong>
                </div>
            @endif
        </div>
    </div>
    <!---------------------------------------------- News  ---------------------------------------------->
@endsection

@push('scripts')
    <!-- Add custom scripts  -->
@endpush