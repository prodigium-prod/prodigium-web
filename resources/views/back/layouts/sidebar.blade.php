<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="sidebar-user-material" style="background-color: #273246; border-radius: 0px 0px 31px 31px;">
                <div class="category-content">
                    <div class="sidebar-user-material-content" style="padding-top: 10px;">
                        <a href=""><img src="{{ url('assets/images/avatar.png') }}" class="img-circle img-responsive" alt="" style="width: 96px; height: 96px; object-fit: fill;"></a>
                        <h6>{{ Auth::user()->name }}</h6>
                        <span class="text-size-small text-slate-300">{{ Auth::user()->email }}</span>
                    </div>
                </div>
            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- --------------------------------------- ADMINISTRATOR --------------------------------------- -->
                    <!-- Main -->
                    <li class="{{ request()->is('administrator/dashboard', 'administrator/dashboard/*') ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main Menu"></i></li>
                    <li>
                        <a href="#"><i class="icon-newspaper2"></i> <span>News</span></a>
                        <ul>
                            <li class="{{ request()->is('administrator/news/add', 'administrator/news/add/*') ? 'active' : '' }}"><a href="{{ route('news.add.form') }}">Add New</a></li>
                            <li class="{{ request()->is('administrator/news/list', 'administrator/news/list/*') ? 'active' : '' }}"><a href="{{ route('news.list') }}">List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-list3"></i> <span>Events</span></a>
                        <ul>
                            <li class="{{ request()->is('administrator/events/add', 'administrator/events/add/*') ? 'active' : '' }}"><a href="{{ route('events.add.form') }}">Add New</a></li>
                            <li class="{{ request()->is('administrator/events/list', 'administrator/events/list/*') ? 'active' : '' }}"><a href="{{ route('events.list') }}">List</a></li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('administrator/tags', 'administrator/tags/*') ? 'active' : '' }}"><a href="{{ route('page.tag') }}"><i class="icon-list2"></i> <span>Tags</span></a></li>

                    <li class="navigation-header"><span>Settings</span> <i class="icon-gear" title="Settings"></i></li>
                    @if(Auth::user()->role == 0)
                    <li class="{{ request()->is('administrator/users', 'administrator/users/*') ? 'active' : '' }}"><a href="{{ route('page.user') }}"><i class="icon-list2"></i> <span>User List</span></a></li>
                    @endif
                    <li class="{{ request()->is('administrator/page-settings') ? 'active' : '' }}"><a href="{{ route('page.settings') }}"><i class="icon-file-text"></i> <span>Page Settings</span></a></li>
                    <!-- <li>
                        <a href="#"><i class="icon-file-text"></i> <span>Pages</span></a>
                        <ul>
                            <li class=""><a href="javascript:;">General</a></li>
                            <li class=""><a href="javascript:;">Landing Page</a></li>
                        </ul>
                    </li> -->
                    <li class="{{ request()->is('administrator/about-us') ? 'active' : '' }}"><a href="{{ route('page.about-us') }}"><i class="icon-office"></i> <span>About Us</span></a></li>
                    <li class="{{ request()->is('administrator/contact') ? 'active' : '' }}"><a href="{{ route('page.contact') }}"><i class="icon-profile"></i> <span>Contact</span></a></li>
                    <li class="{{ request()->is('administrator/faq') ? 'active' : '' }}"><a href="{{ route('page.faq') }}"><i class="icon-file-presentation2"></i> <span>FAQ</span></a></li>
                    <li class="{{ request()->is('administrator/subscribe') ? 'active' : '' }}"><a href="{{ route('page.subscribe') }}"><i class="icon-list-ordered"></i> <span>Subscribe</span></a></li>
                    <!-- --------------------------------------- ADMINISTRATOR --------------------------------------- -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>