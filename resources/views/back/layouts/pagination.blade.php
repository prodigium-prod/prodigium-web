<div align="right">
    @if ($paginator->hasPages())
        <ul class="pagination pagination-flat pagination-rounded">
        
            @if ($paginator->onFirstPage())
                <li class="disabled"><a>&lsaquo;</a></li>
            @else
                <li><a href="{{ $paginator->previousPageUrl() }}" rel="pref">&lsaquo;</a></li>
            @endif


            @foreach ($elements as $element)
            
                @if (is_string($element))
                    <li class="disabled"><a href="#">{{ $element }}</a></li>
                @endif
            
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a href="#" style="background: #C9454A;">{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach


            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&rsaquo;</a></li>
            @else
                <li class="disabled"><a>&rsaquo;</a></li>
            @endif

        </ul>
    @else

        <ul class="pagination pagination-flat pagination-rounded">
            <li class="disabled"><a>&lsaquo;</a></li>
            <li class="active"><a>1</a></li>
            <li class="disabled"><a>&rsaquo;</a></li>
        </ul>
    @endif
</div>