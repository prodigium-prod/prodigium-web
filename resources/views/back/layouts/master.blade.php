<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }}</title>
    <link rel="icon" href="{{ url('assets/images/icon.png') }}" type="image/gif" sizes="16x16">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/visualization/c3/c3.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/notifications/bootbox.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/pagination/bs_pagination.min.js') }}"></script>

	<!-- <script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script> -->
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/media/fancybox.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/notifications/pnotify.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/ui/fab.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/ui/prism.min.js') }}"></script>

	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/components_thumbnails.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/form_input_groups.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/datatables_extension_colvis.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/editor_wysihtml5.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/datatables_advanced.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/uploader_bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/gallery.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/components_notifications_pnotify.js') }}"></script>

	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /theme JS files -->

	<style>
		@media screen and (min-height: 720px) {
			.cusom-height-conversation {
				min-height: 57vh;
			}
		}
		
		@media screen and (min-height: 1080px) {
			.cusom-height-conversation {
				min-height: 71vh;
			}
		}
        
        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px #7c7c7c; 
            border-radius: 10px;
        }
        
        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #2c2a3400; 
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #0d6efd; 
        }
	</style>

</head>

<body class="navbar-bottom">

	<!-- Main navbar -->
    @include('back.layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header" style="margin-bottom: 19px;">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				{!! $breadcrumb !!}
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="mailto:restujulian07@gmail.com" target="_blank"><i class="icon-comment-discussion position-left"></i> Support</a></li>
			</ul>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
            @include('back.layouts.sidebar')
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
                @yield('content')
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
    <div class="navbar navbar-default navbar-fixed-bottom footer">
        <ul class="nav navbar-nav visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="footer">
            <div class="navbar-text">
                {{ config('app.name') }} | &copy; {{ date('Y') }}
            </div>

            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li><a data-toggle="modal" data-target="#version_info">{{ pageSetup()->version_code }}</a></li>
                </ul>
            </div>
        </div>
    </div>
	<!-- /footer -->

	<!-- Change icon modal -->
	<div id="version_info" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-slate-700">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title"><i class="icon-info22"></i> &nbsp;Version : {{ pageSetup()->version_code }}</h5>
				</div>

				<div class="modal-body">
					{!! pageSetup()->version_detail !!}
				</div>

				<div class="modal-footer">
					<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /Change icon modal -->
</body>
</html>
