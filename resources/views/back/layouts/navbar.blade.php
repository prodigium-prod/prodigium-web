<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('dashboard') }}">
            <div style="font-size: 23px; font-weight: 700;">
                <span>{{ strtoupper(config('app.name')) }}</span>
            </div>
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
                <a href="#">
                    <span>Hi ! {{ Auth::user()->name }}</span>
                </a>
            </li>
            <li>
                <a id="sweet_logout" class="pointer dropdown-item">
                    <i class="fa fa-power-off" aria-hidden="true" style="font-size: 22px;"></i>
                </a>

                <script>
                    $(function() {
                        // Warning alert logout
                        $('#sweet_logout').on('click', function() {
                            swal({
                                title: "Are you sure?",
                                text: "You will be logged out, logged back in to regain access!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#EF5350",
                                confirmButtonText: "Yes, Logout!",
                                cancelButtonText: "No, cancel!",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm){
                                if (isConfirm) {
                                    window.location.href = "{{ route('logout') }}";
                                }
                                else {
                                    swal({
                                        title: "Cancelled",
                                        text: "You still have this access :)",
                                        confirmButtonColor: "#2196F3",
                                        type: "error"
                                    });
                                }
                            });
                        });
                    });
                </script>
            </li>
        </ul>
    </div>
</div>