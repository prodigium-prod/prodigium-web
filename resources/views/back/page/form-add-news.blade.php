@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>Add New</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <form action="{{ route('news.add.form.action') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Banner <span class="text-danger">*</span></strong>
                                </label>
                                <input type="file" class="file-input" id="first-name" name="file" accept="image/*" required data-show-caption="false" data-show-upload="false">
                                <span class="help-block">
                                    <ul>
                                        <li>The maximum size is 2MB.</li>
                                        <li>Supported formats JPG/JPEG or PNG.</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-8 row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label><strong>Tags <span class="text-danger">*</span></strong></label>
                                    <div class="row">
                                        <div class="col-md-10 col-9">
                                            <select multiple="multiple" name="tags[]" data-placeholder="Select Tags..." required="required" class="select-menu2-color">
                                                <optgroup label="Tag">
                                                    @foreach($tags as $value)
                                                        <option value="{{ $value->id }}">{{ $value->tag }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-3y">
                                            <a href="{{ route('page.tag') }}" class="btn bg-blue-700 btn-xlg btn-labeled legitRipple" style="height:50px; width: 100%;  padding-top: 15px;"><b><i class="icon-plus3" style="height: 50px; padding-top: 3px;"></i></b> Add Tag</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        <strong>Keywords (meta) <span class="text-danger">*</span></strong>
                                    </label>
                                    <textarea rows="2" cols="5" name="keywords" class="form-control" required="required" placeholder="Enter text..."></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                <strong>Description ID (meta) <span class="text-danger">*</span></strong>
                                            </label>
                                            <textarea rows="3" cols="5" name="description_id" class="form-control" required="required" placeholder="Enter text..."></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                <strong>Description EN (meta) <span class="text-danger">*</span></strong>
                                            </label>
                                            <textarea rows="3" cols="5" name="description_en" class="form-control" required="required" placeholder="Enter text..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Title (ID) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea rows="1" cols="5" name="title_id" class="form-control" required="required" placeholder="Enter text..."></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Title (EN) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea rows="1" cols="5" name="title_en" class="form-control" required="required" placeholder="Enter text..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Content (ID) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea cols="18" rows="18" name="content_id" class="wysihtml5 wysihtml5-min form-control" required="required" placeholder="Enter text ..."></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Content (ID) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea cols="18" rows="18" name="content_en" class="wysihtml5 wysihtml5-min form-control" required="required" placeholder="Enter text ..."></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="pt-10 mt-10 text-right">
                        <!-- <a href="{{ route('news', app()->getLocale()) }}" target="_blank" class="btn bg-blue btn-labeled legitRipple"><b><i class="icon-browser"></i></b> Landing Page</a> -->
                        <button type="submit" class="btn bg-teal-400 btn-labeled legitRipple"><b><i class="glyphicon glyphicon-saved"></i></b> Save & Changes</button>
                    </div>
                </form>
			</div>
		</div>

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection