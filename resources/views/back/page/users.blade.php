@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>User List</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <button type="button" data-toggle="modal" data-target="#model_add_new" class="btn bg-slate-400 btn-labeled"><b><i class="icon-plus-circle2"></i></b> New User</button>

                <!-- Modal -->
                <div id="model_add_new" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-slate-700">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h6 class="modal-title">New User</h6>
                            </div>

                            <form action="{{ route('page.user.add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label><strong>Group:</strong></label>
                                        <input type="text" class="form-control" name="group" value="admin" placeholder="Type the group..." readonly>
                                    </div>
                                    <div class="form-group">
                                        <label><strong>Name:</strong></label>
                                        <input type="text" class="form-control" name="name" value="" placeholder="Type the name..." required>
                                    </div>
                                    <div class="form-group">
                                        <label><strong>Email:</strong></label>
                                        <input type="email" class="form-control" name="email" value="" placeholder="example@mail.com" required>
                                    </div>
                                    <div class="form-group">
                                        <label><strong>Password:</strong></label>
                                        <input type="password" class="form-control" name="password" value="" placeholder="Type the password..." required>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn bg-slate-700">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /Modal -->

                <div class="table-responsive">
                    <table class="table datatable-colvis-basic">
                        <thead>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Email</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = x_number($users->currentPage(), $users->perPage());
                            @endphp
                            @foreach ($users as $s_user)
                            <tr>
                                <td style="vertical-align: top;">{{ $no++ }}</td>
                                <td style="vertical-align: top;">{{ $s_user->name }}</td>
                                <td style="vertical-align: top;">{{ $s_user->role == 0 ? "superadmin" : "admin" }}</td>
                                <td style="vertical-align: top;">{{ $s_user->email }}</td>
                                <td class="text-center" style="vertical-align: top; width: 150px;">
                                    <form action="{{ route('page.user.delete', $s_user->id) }}" id="del_item{{ $s_user->id }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        @if($s_user->role == 0)
                                            <button type="button" class="btn bg-warning-600 btn-icon btn-rounded legitRipple" disabled><i class="icon-trash"></i></button>
                                        @else
                                            <button type="button" id="sweet_delete{{ $s_user->id }}" class="btn bg-warning-600 btn-icon btn-rounded legitRipple"><i class="icon-trash"></i></button>
                                        @endif
                                        <button type="button" class="btn bg-primary btn-icon btn-rounded legitRipple" data-toggle="modal" data-target="#edit{{ $s_user->id }}"><i class="icon-pencil5"></i></button>
                                    </form>
                                    <script>    
                                        $(function() {
                                            // Warning alert delete
                                            $('#sweet_delete{{ $s_user->id }}').on('click', function() {
                                                swal({
                                                    title: "Are you sure?",
                                                    text: "The data you select will be deleted from the database!",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#EF5350",
                                                    confirmButtonText: "Yes, Delete!",
                                                    cancelButtonText: "No, cancel!",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: false
                                                },
                                                function(isConfirm){
                                                    if (isConfirm) {
                                                        document.getElementById("del_item{{ $s_user->id }}").submit();
                                                    }
                                                    else {
                                                        swal({
                                                            title: "Cancelled",
                                                            text: "Deletion canceled :)",
                                                            confirmButtonColor: "#2196F3",
                                                            type: "error"
                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    </script>
                                    
                                    <!-- Iconified modal -->
                                    <div id="edit{{ $s_user->id }}" class="modal fade">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content text-left">
                                                <div class="modal-header bg-primary">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-pencil5"></i> &nbsp;Edit FAQ</h5>
                                                </div>

                                                <form action="{{ route('page.user.update', $s_user->id) }}" method="POST" enctype="multipart/form-data" style="padding: 20px;">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label><strong>Group:</strong></label>
                                                            <input type="text" class="form-control" name="group" value="admin" placeholder="Type the group..." readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label><strong>Name:</strong></label>
                                                            <input type="text" class="form-control" name="name"  value="{{ $s_user->name }}" placeholder="Type the name..." required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label><strong>Email:</strong></label>
                                                            <input type="email" class="form-control" name="email"  value="{{ $s_user->email }}" placeholder="example@mail.com" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label><strong>Password:</strong></label>
                                                            <input type="password" class="form-control" name="password"  value="secret" placeholder="Type the password..." required>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                                                        <button type="submit" class="btn btn-primary"><i class="icon-check"></i> Save</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /iconified modal -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Email</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $users->links('back.layouts.pagination') }}
			</div>
		</div>

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection