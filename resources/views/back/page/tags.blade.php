@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>Tags</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <button type="button" data-toggle="modal" data-target="#model_add_new" class="btn bg-slate-400 btn-labeled"><b><i class="icon-plus-circle2"></i></b> New Tag</button>

                <!-- Modal -->
                <div id="model_add_new" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-slate-700">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h6 class="modal-title">New Tag</h6>
                            </div>

                            <form action="{{ route('page.tag.add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label><strong>Tag Name:</strong></label>
                                        <input type="text" class="form-control" name="tag" value="" placeholder="Type the tag..." required>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn bg-slate-700">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /Modal -->

                <div class="table-responsive">
                    <table class="table datatable-colvis-basic">
                        <thead>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Tags</th>
                                <th>Date</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = x_number($tags->currentPage(), $tags->perPage());
                            @endphp
                            @foreach ($tags as $s_tag)
                            <tr>
                                <td style="vertical-align: top;">{{ $no++ }}</td>
                                <td style="vertical-align: top;">{{ $s_tag->tag }}</td>
                                <td style="vertical-align: top;">{{ date('d M Y H:i A', strtotime($s_tag->created_at)) }}</td>
                                <td class="text-center" style="vertical-align: top; width: 150px;">
                                    <form action="{{ route('page.tag.delete', $s_tag->id) }}" id="del_item{{ $s_tag->id }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <button type="button" id="sweet_delete{{ $s_tag->id }}" class="btn bg-warning-600 btn-icon btn-rounded legitRipple"><i class="icon-trash"></i></button>
                                        <button type="button" class="btn bg-primary btn-icon btn-rounded legitRipple" data-toggle="modal" data-target="#edit{{ $s_tag->id }}"><i class="icon-pencil5"></i></button>
                                    </form>
                                    <script>    
                                        $(function() {
                                            // Warning alert delete
                                            $('#sweet_delete{{ $s_tag->id }}').on('click', function() {
                                                swal({
                                                    title: "Are you sure?",
                                                    text: "The data you select will be deleted from the database!",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#EF5350",
                                                    confirmButtonText: "Yes, Delete!",
                                                    cancelButtonText: "No, cancel!",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: false
                                                },
                                                function(isConfirm){
                                                    if (isConfirm) {
                                                        document.getElementById("del_item{{ $s_tag->id }}").submit();
                                                    }
                                                    else {
                                                        swal({
                                                            title: "Cancelled",
                                                            text: "Deletion canceled :)",
                                                            confirmButtonColor: "#2196F3",
                                                            type: "error"
                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    </script>
                                    
                                    <!-- Iconified modal -->
                                    <div id="edit{{ $s_tag->id }}" class="modal fade">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content text-left">
                                                <div class="modal-header bg-primary">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-pencil5"></i> &nbsp;Edit FAQ</h5>
                                                </div>

                                                <form action="{{ route('page.tag.update', $s_tag->id) }}" method="POST" enctype="multipart/form-data" style="padding: 20px;">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label><strong>Tag Name:</strong></label>
                                                            <input type="text" class="form-control" name="tag" value="{{ $s_tag->tag }}" placeholder="Type the tag..." required>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                                                        <button type="submit" class="btn btn-primary"><i class="icon-check"></i> Save</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /iconified modal -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Tags</th>
                                <th>Date</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $tags->links('back.layouts.pagination') }}
			</div>
		</div>

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection