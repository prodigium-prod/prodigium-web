@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>Subscribe</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">

                <div class="table-responsive">
                    <table class="table datatable-colvis-basic">
                        <thead>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = x_number($subscribe->currentPage(), $subscribe->perPage());
                            @endphp
                            @foreach ($subscribe as $s_subs)
                            <tr>
                                <td style="vertical-align: top;">{{ $no++ }}</td>
                                <td style="vertical-align: top;">{{ $s_subs->email }}</td>
                                <td style="vertical-align: top;">{{ date('d M Y H:i A', strtotime($s_subs->created_at)) }}</td>
                                <td class="text-center" style="vertical-align: top; width: 150px;">
                                    <form action="{{ route('page.faq.delete', $s_subs->id) }}" id="del_item{{ $s_subs->id }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <button type="button" id="sweet_delete{{ $s_subs->id }}" class="btn bg-warning-600 btn-icon btn-rounded legitRipple"><i class="icon-trash"></i></button>
                                        <a href="mailto:{{ $s_subs->email }}" class="btn bg-primary btn-icon btn-rounded legitRipple"><i class="icon-envelope"></i></a>
                                    </form>
                                    <script>    
                                        $(function() {
                                            // Warning alert delete
                                            $('#sweet_delete{{ $s_subs->id }}').on('click', function() {
                                                swal({
                                                    title: "Are you sure?",
                                                    text: "The data you select will be deleted from the database!",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#EF5350",
                                                    confirmButtonText: "Yes, Delete!",
                                                    cancelButtonText: "No, cancel!",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: false
                                                },
                                                function(isConfirm){
                                                    if (isConfirm) {
                                                        document.getElementById("del_item{{ $s_subs->id }}").submit();
                                                    }
                                                    else {
                                                        swal({
                                                            title: "Cancelled",
                                                            text: "Deletion canceled :)",
                                                            confirmButtonColor: "#2196F3",
                                                            type: "error"
                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    </script>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $subscribe->links('back.layouts.pagination') }}
			</div>
		</div>

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection