@extends("back.layouts.master")

@section("content")
	<!-- Dashboard content -->			
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Activities</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body text-center">
                    <div class="display-inline-block" id="top-7product"></div>
                    <script>
                        var donut_chart = c3.generate({
                            bindto: '#top-7product',
                            size: { width: 194, height: 194, },
                            color: {
                                pattern: ['#5b8e37', '#107ec0', '#425158']
                            },
                            data: {
                                columns: [],
                                type : 'pie'
                            },
                            donut: {
                                title: "Activities"
                            }
                        });

                        // Change data
                        setTimeout(function () {
                            donut_chart.load({
                                columns: [
                                    ['News : {{ $news_total }}', {{ $news_total }}], ['Events : 1', 1],
                                ]
                            });
                        }, 17);
                    </script>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <!-- Quick stats boxes -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-body bg-green-800 has-bg-image">
                        <div class="media no-margin-top content-group">
                            <div class="media-left media-middle">
                                <i class="icon-store icon-2x"></i>
                            </div>

                            <div class="media-body">
                                <h6 class="no-margin text-semibold">News</h6>
                                <span class="text-muted">{{ date('l, M j Y') }}</span>
                            </div>
                        </div>

                        <div class="progress progress-micro mb-10 bg-primary-400">
                            <div class="progress-bar bg-white" style="width: 100%">
                                <span class="sr-only">{{ $news_total }} News</span>
                            </div>
                        </div>
                        <span class="pull-right">{{ $news_total }}</span>
                        Total News
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-body bg-blue-800 has-bg-image">
                        <div class="media no-margin-top content-group">
                            <div class="media-left media-middle">
                                <i class="icon-price-tags icon-2x"></i>
                            </div>

                            <div class="media-body">
                                <h6 class="no-margin text-semibold">Events</h6>
                                <span class="text-muted">{{ date('l, M j Y') }}</span>
                            </div>
                        </div>

                        <div class="progress progress-micro mb-10 bg-primary-400">
                            <div class="progress-bar bg-white" style="width: 100%">
                                <span class="sr-only">1 Events</span>
                            </div>
                        </div>
                        <span class="pull-right">1</span>
                        Total Events
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-body bg-pink-700 has-bg-image">
                        <div class="media no-margin-top content-group">
                            <div class="media-left media-middle">
                                <i class="icon-stack-check icon-2x"></i>
                            </div>

                            <div class="media-body">
                                <h6 class="no-margin text-semibold">Tags</h6>
                                <span class="text-muted">{{ date('l, M j Y') }}</span>
                            </div>
                        </div>

                        <div class="progress progress-micro mb-10 bg-primary-400">
                            <div class="progress-bar bg-white" style="width: 100%">
                                <span class="sr-only">{{ $tag_total }} Tags</span>
                            </div>
                        </div>
                        <span class="pull-right">{{ $tag_total }}</span>
                        Total Tags
                    </div>
                </div>
            </div>
            <!-- /quick stats boxes -->
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Trending</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table text-nowrap">
                <thead>
                    <tr style="background-color: #e4e4e4;">
                        <th>No.</th>
                        <th class="col-md-3">Title ID</th>
                        <th class="col-md-3">Title EN</th>
                        <th>Keywords</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($news as $key => $value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $value->title_id }}</td>
                            <td>{{ $value->title_en }}</td>
                            <td>{{ $value->meta_keyword }}</td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn bg-teal-400 btn-labeled btn-rounded legitRipple"><b><i class="icon-earth"></i></b> Show</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
	<!-- /dashboard content -->

	@if (Session::has('message_flash'))
		<script>
			swal({
				title: "success",
				text: "{{ Session::get('message_flash') }}",
				confirmButtonColor: "#2196F3",
				type: "success"
			});
		</script>
	@elseif(Session::has('message_flash_failed'))
		<script>
			swal({
				title: "Failed Add",
				text: "{{ Session::get('message_flash_failed') }}",
				confirmButtonColor: "#D32F2F",
				type: "error"
			});
		</script>
	@endif
@endsection