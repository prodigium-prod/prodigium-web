@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>About Us</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <form action="{{ route('page.about-us.save', $aboutUs->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-2">
							<div class="thumbnail">
								<div class="thumb" style="background: #dfe7eb; padding: 31px; border-radius: 17px 17px 0px 0px;">
									<img src="{{ isset($aboutUs->banner) ? url($aboutUs->banner) : url('assets/back/assets/images/placeholder.jpg') }}" style="border-radius: 7px;" alt="">
									<div class="caption-overflow" style="border-radius: 17px 17px 0px 0px;">
										<span>
                                            <a href="{{ isset($aboutUs->banner) ? url($aboutUs->banner) : url('assets/back/assets/images/placeholder.jpg') }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded" style="margin-right: 7px;"><i class="icon-zoomin3"></i></a>
											<a href="javascript:;" data-toggle="modal" data-target="#change_banner" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-pencil5"></i></a>
										</span>
									</div>
								</div>

								<div class="caption text-center bg-slate-300" style="border-radius: 0px 0px 17px 17px; padding: 7px;">
                                    <span class="text-bold text-white">Banner</span>
								</div>
							</div>
                        </div>
                        <div class="col-md-10 row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <strong>Simple (ID) <span class="text-danger">*</span></strong>
                                    </label>
                                    <textarea rows="3" cols="5" name="simple_id" class="form-control" required="required" placeholder="Enter text...">{{ $aboutUs->simple_id }}</textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label">
                                        <strong>Details (ID) <span class="text-danger">*</span></strong>
                                    </label>
                                    <textarea cols="18" rows="18" name="content_id" class="wysihtml5 wysihtml5-min form-control" placeholder="Enter text ...">
                                        {{ $aboutUs->content_id }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <strong>Simple (EN) <span class="text-danger">*</span></strong>
                                    </label>
                                    <textarea rows="3" cols="5" name="simple_en" class="form-control" required="required" placeholder="Enter text...">{{ $aboutUs->simple_en }}</textarea>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label">
                                        <strong>Details (EN) <span class="text-danger">*</span></strong>
                                    </label>
                                    <textarea cols="18" rows="18" name="content_en" class="wysihtml5 wysihtml5-min form-control" placeholder="Enter text ...">
                                        {{ $aboutUs->content_en }}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="pt-10 mt-10 text-right">
                        <a href="{{ route('about-us', app()->getLocale()) }}" target="_blank" class="btn bg-blue btn-labeled legitRipple"><b><i class="icon-browser"></i></b> Landing Page</a>
                        <button type="submit" class="btn bg-teal-400 btn-labeled legitRipple"><b><i class="glyphicon glyphicon-saved"></i></b> Save & Changes</button>
                    </div>
                </form>
			</div>
		</div>

        <!-- Change banner modal -->
        <div id="change_banner" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-slate-700">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><i class="icon-pencil5"></i> &nbsp;Change Banner</h5>
                    </div>

                    <form action="{{ route('page.about-us.change.banner', $aboutUs->id) }}" method="POST" enctype="multipart/form-data" style="padding: 20px;">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Banner <span class="text-danger">*</span></strong>
                                </label>
                                <input type="file" class="file-input" id="first-name" name="file" accept="image/*" required data-show-caption="false" data-show-upload="false">
                                <span class="help-block">
                                    <ul>
                                        <li>The maximum size is 2MB.</li>
                                        <li>Supported formats JPG/JPEG or PNG.</li>
                                    </ul>
                                </span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                            <button type="submit" class="btn bg-slate-700"><i class="icon-check"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Change icon modal -->

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection