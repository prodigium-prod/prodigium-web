@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>Page Settings</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <form action="{{ route('page.settings.save', $pages->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">
                            <strong>Headline <span class="text-danger">*</span></strong>
                        </label>
                        <textarea rows="2" cols="5" name="headline" class="form-control" required="required" placeholder="Type headline...">{{ isset($pages->headline) ? $pages->headline : '' }}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
							<div class="thumbnail">
								<div class="thumb" style="background: #dfe7eb; padding: 31px; border-radius: 17px 17px 0px 0px;">
									<img src="{{ isset($pages->meta_icon) ? url($pages->meta_icon) : url('assets/back/assets/images/placeholder.jpg') }}" style="height: 60px; width: auto;" alt="">
									<div class="caption-overflow" style="border-radius: 17px 17px 0px 0px;">
										<span>
                                            <a href="{{ isset($pages->meta_icon) ? url($pages->meta_icon) : url('assets/back/assets/images/placeholder.jpg') }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded" style="margin-right: 7px;"><i class="icon-zoomin3"></i></a>
											<a href="javascript:;" data-toggle="modal" data-target="#change_icon" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-pencil5"></i></a>
										</span>
									</div>
								</div>

								<div class="caption text-center bg-slate-300" style="border-radius: 0px 0px 17px 17px; padding: 7px;">
                                    <span class="text-bold text-white">ICON</span>
								</div>
							</div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Title (meta) <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="meta_title" class="form-control" value="{{ isset($pages->meta_title) ? $pages->meta_title : '' }}" required="required" placeholder="Text input validation">
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Keywords (meta) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea rows="2" cols="5" name="meta_keywords" class="form-control" required="required" placeholder="Default textarea">{{ isset($pages->meta_keywords) ? $pages->meta_keywords : '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Descriptions (meta) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea rows="5" cols="5" name="meta_descriptions" class="form-control" required="required" placeholder="Default textarea">{{ isset($pages->meta_descriptions) ? $pages->meta_descriptions : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-2">
							<div class="thumbnail">
								<div class="thumb" style="background: #dfe7eb; padding: 31px; border-radius: 17px 17px 0px 0px;">
									<img src="{{ isset($pages->logo_image) ? url($pages->logo_image) : url('assets/back/assets/images/placeholder.jpg') }}" alt="">
									<div class="caption-overflow" style="border-radius: 17px 17px 0px 0px;">
										<span>
                                            <a href="{{ isset($pages->logo_image) ? url($pages->logo_image) : url('assets/back/assets/images/placeholder.jpg') }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded" style="margin-right: 7px;"><i class="icon-zoomin3"></i></a>
											<a href="javascript:;" data-toggle="modal" data-target="#change_logo" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-pencil5"></i></a>
										</span>
									</div>
								</div>

								<div class="caption text-center bg-slate-300" style="border-radius: 0px 0px 17px 17px; padding: 7px;">
                                    <span class="text-bold text-white">LOGO</span>
								</div>
							</div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Logo (ALT) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea rows="5" cols="5" name="logo_alt" class="form-control" required="required" placeholder="Type alt (logo)...">{{ isset($pages->logo_alt) ? $pages->logo_alt : '' }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Logo (descriptions) <span class="text-danger">*</span></strong>
                                </label>
                                <textarea rows="5" cols="5" name="logo_descriptions" class="form-control" required="required" placeholder="Type descriptions (logo)...">{{ isset($pages->logo_descriptions) ? $pages->logo_descriptions : '' }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Version Code <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="version_code" class="form-control" value="{{ isset($pages->version_code) ? $pages->version_code : '' }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">
                            <strong>Version Details <span class="text-danger">*</span></strong>
                        </label>
                        <textarea cols="7" rows="18" name="version_detail" class="wysihtml5 wysihtml5-min form-control" placeholder="Enter text ...">
                            {{ isset($pages->version_detail) ? $pages->version_detail : '' }}
                        </textarea>
                    </div>

                    <div class="pt-10 mt-10 text-right">
                        <a href="{{ route('main', app()->getLocale()) }}" target="_blank" class="btn bg-blue btn-labeled legitRipple"><b><i class="icon-browser"></i></b> Landing Page</a>
                        <button type="submit" class="btn bg-teal-400 btn-labeled legitRipple"><b><i class="glyphicon glyphicon-saved"></i></b> Save & Changes</button>
                    </div>
                </form>
			</div>
		</div>

        <!-- Change icon modal -->
        <div id="change_icon" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-slate-700">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><i class="icon-pencil5"></i> &nbsp;Change Icon</h5>
                    </div>

                    <form action="{{ route('page.settings.change.file', ['id' => $pages->id, 'type' => 'icon']) }}" method="POST" enctype="multipart/form-data" style="padding: 20px;">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Icon (meta) <span class="text-danger">*</span></strong>
                                </label>
                                <input type="file" class="file-input" id="first-name" name="file" accept="image/*" required data-show-caption="false" data-show-upload="false">
                                <span class="help-block">
                                    <ul>
                                        <li>The maximum size is 2MB.</li>
                                        <li>Supported formats JPG/JPEG or PNG.</li>
                                    </ul>
                                </span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                            <button type="submit" class="btn bg-slate-700"><i class="icon-check"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Change icon modal -->
        
        <!-- Change Logo modal -->
        <div id="change_logo" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-slate-700">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><i class="icon-pencil5"></i> &nbsp;Change Logo</h5>
                    </div>

                    <form action="{{ route('page.settings.change.file', ['id' => $pages->id, 'type' => 'logo']) }}" method="POST" enctype="multipart/form-data" style="padding: 20px;">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Logo (Images)<span class="text-danger">*</span></strong>
                                </label>
                                <input type="file" class="file-input" id="first-name" name="file" accept="image/*" required data-show-caption="false" data-show-upload="false">
                                <span class="help-block">
                                    <ul>
                                        <li>The maximum size is 2MB.</li>
                                        <li>Supported formats JPG/JPEG or PNG.</li>
                                    </ul>
                                </span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                            <button type="submit" class="btn bg-slate-700"><i class="icon-check"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Change Logo modal -->

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection