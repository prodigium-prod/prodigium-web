@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>Events (All)</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <a href="{{ route('events.add.form') }}" class="btn bg-slate-400 btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add New</a>

                <div class="table-responsive">
                    <table class="table datatable-colvis-basic">
                        <thead>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Title ID</th>
                                <th>Title EN</th>
                                <th>Keywords</th>
                                <th>Views</th>
                                <th>Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = x_number($events->currentPage(), $events->perPage());
                            @endphp
                            @foreach ($events as $s_event)
                            <tr>
                                <td style="vertical-align: top;">{{ $no++ }}</td>
                                <td style="vertical-align: top;">{{ $s_event->title_id }}</td>
                                <td style="vertical-align: top;">{{ $s_event->title_en }}</td>
                                <td style="vertical-align: top;">{{ $s_event->meta_keyword }}</td>
                                <td style="vertical-align: top;">{{ $s_event->views }}</td>
                                <td style="vertical-align: top;">
                                    @if($s_event->status == 1)
                                        <a href="{{ route('events.list.change.status', ['id' => $s_event->id, 'status' => 0]) }}" class="btn bg-teal-400 btn-labeled btn-rounded legitRipple"><b><i class="icon-shield-check"></i></b> Active</a>
                                    @else
                                        <a href="{{ route('events.list.change.status', ['id' => $s_event->id, 'status' => 1]) }}" class="btn bg-danger-400 btn-labeled btn-rounded legitRipple"><b><i class="icon-shield-notice"></i></b> De-Active</a>
                                    @endif
                                </td>
                                <td class="text-center" style="vertical-align: top; width: 162px;">
                                    <form action="{{ route('events.list.delete', $s_event->id) }}" id="del_item{{ $s_event->id }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <a href="{{ route('events-detail', ['lang' => app()->getLocale(), 'code' => $s_event->event_code, 'slug' => $s_event->slug(app()->getLocale()) ? $s_event->slug(app()->getLocale()) : '-']) }}" target="_blank" class="btn bg-primary btn-icon btn-rounded legitRipple"><i class="icon-earth"></i></a>
                                        <a href="{{ route('events.list.detail.edit', $s_event->id) }}" class="btn bg-teal btn-icon btn-rounded legitRipple"><i class="icon-pencil7"></i></a>
                                        <button type="button" id="sweet_delete{{ $s_event->id }}" class="btn bg-warning-600 btn-icon btn-rounded legitRipple"><i class="icon-trash"></i></button>
                                    </form>
                                    <script>    
                                        $(function() {
                                            // Warning alert delete
                                            $('#sweet_delete{{ $s_event->id }}').on('click', function() {
                                                swal({
                                                    title: "Are you sure?",
                                                    text: "The data you select will be deleted from the database!",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#EF5350",
                                                    confirmButtonText: "Yes, Delete!",
                                                    cancelButtonText: "No, cancel!",
                                                    closeOnConfirm: false,
                                                    closeOnCancel: false
                                                },
                                                function(isConfirm){
                                                    if (isConfirm) {
                                                        document.getElementById("del_item{{ $s_event->id }}").submit();
                                                    }
                                                    else {
                                                        swal({
                                                            title: "Cancelled",
                                                            text: "Deletion canceled :)",
                                                            confirmButtonColor: "#2196F3",
                                                            type: "error"
                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    </script>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #e4e4e4;">
                                <th style="width: 5%;">No.</th>
                                <th>Title ID</th>
                                <th>Title EN</th>
                                <th>Keywords</th>
                                <th>Views</th>
                                <th>Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $events->links('back.layouts.pagination') }}
			</div>
		</div>

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection