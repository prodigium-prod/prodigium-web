@extends("back.layouts.master")

	@section("content")
		<div class="panel panel-white border-top-blue">
			<div class="panel-heading">
				<h6 class="panel-title"><b>About Us</b></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
                <form action="{{ route('page.contact.save', $contact->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>LinkedIn <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="linkedin" class="form-control" value="{{ $contact->linkedin }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Google + <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="google" class="form-control" value="{{ $contact->google }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Twitter <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="twitter" class="form-control" value="{{ $contact->twitter }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Instagram <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="instagram" class="form-control" value="{{ $contact->instagram }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Facebook <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="facebook" class="form-control" value="{{ $contact->facebook }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Youtube <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="youtube" class="form-control" value="{{ $contact->youtube }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Phone Number <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="phone_number" class="form-control" value="{{ $contact->phone_number }}" required="required" placeholder="+62 80000000">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Email <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="email" class="form-control" value="{{ $contact->email }}" required="required" placeholder="example@mail.com">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Address <span class="text-danger">*</span></strong>
                                </label>
                                <input type="text" name="address" class="form-control" value="{{ $contact->address }}" required="required" placeholder="Text input validation">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">
                            <strong>Maps Embed <span class="text-danger">(URL only from Google Maps) *</span></strong>
                        </label>
                        <textarea rows="3" cols="5" name="maps_embed" class="form-control" required="required" placeholder="Maps (Embed)...">{{ $contact->maps_embed }}</textarea>
                        <span class="help-block">Take this section : {{ 'src="' }}<code>https://www.google.com/maps/embed?...</code>"</span>
                    </div>
                    
                    <div class="pt-10 mt-10 text-right">
                        <a href="{{ route('contact', app()->getLocale()) }}" target="_blank" class="btn bg-blue btn-labeled legitRipple"><b><i class="icon-browser"></i></b> Landing Page</a>
                        <button type="submit" class="btn bg-teal-400 btn-labeled legitRipple"><b><i class="glyphicon glyphicon-saved"></i></b> Save & Changes</button>
                    </div>
                </form>
			</div>
		</div>

        <!-- Change banner modal -->
        <div id="change_banner" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-slate-700">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><i class="icon-pencil5"></i> &nbsp;Change Banner</h5>
                    </div>

                    <form action="" method="POST" enctype="multipart/form-data" style="padding: 20px;">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">
                                    <strong>Banner <span class="text-danger">*</span></strong>
                                </label>
                                <input type="file" class="file-input" id="first-name" name="file" accept="image/png, image/gif, image/jpeg image/jpg" required data-show-caption="false" data-show-upload="false">
                                <span class="help-block">
                                    <ul>
                                        <li>The maximum size is 2MB.</li>
                                        <li>Supported formats JPG/JPEG or PNG.</li>
                                    </ul>
                                </span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                            <button type="submit" class="btn bg-slate-700"><i class="icon-check"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Change icon modal -->

        @if (Session::has('message_flash'))
            <script>
                swal({
                    title: "success",
                    text: "{{ Session::get('message_flash') }}",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                });
            </script>
        @elseif(Session::has('message_flash_failed'))
            <script>
                swal({
                    title: "Failed Add",
                    text: "{{ Session::get('message_flash_failed') }}",
                    confirmButtonColor: "#D32F2F",
                    type: "error"
                });
            </script>
        @endif
	@endsection