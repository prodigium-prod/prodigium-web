<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }} - Administrator Area</title>
    <link rel="icon" href="{{ url('assets/images/icon.png') }}" type="image/gif" sizes="16x16">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/back/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

	<script type="text/javascript" src="{{ url('assets/back/assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/back/assets/js/pages/login.js') }}"></script>

	<script type="text/javascript" src="{{ url('assets/back/assets/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container" style="background-color: #222222;">

	<!-- Page container -->
	<div class="page-container pb-20">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Advanced login -->
				<form action="{{ route('authentication.process') }}" method="post" class="form-validate">
					@method('POST')
                    @csrf
					<div class="panel panel-body login-form" style="box-shadow: 7px 5px 12px 5px #b5b5b552; border-radius: 17px;">
						<div class="text-center">
							<div class="icon-object" style="width: 90px; height: 90px; padding-top: 27px; color: #5865f2;"><i class="icon-users4"></i></div>
							<h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
						</div>

                        @if(session('errors'))
                            <div class="alert bg-danger alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                <!-- <span class="text-semibold">Well done!</span> You successfully read <a href="#" class="alert-link">this important</a> alert message. -->
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert bg-success alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert bg-danger alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                {{ Session::get('error') }}
                            </div>
                        @endif
						
						<div class="form-group has-feedback has-feedback-left">
							<input type="email" name="email" class="form-control" placeholder="example@prodigy-id.com">
							<div class="form-control-feedback">
								<i class="icon-mention text-muted"></i>
							</div>
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<div class="input-group" id="show_hide_password">
								<input type="password" class="form-control" name="password" placeholder="Type password.." required>
								<span class="input-group-addon"><i class="icon-eye-blocked"></i></span>
							</div>
							<div class="form-control-feedback">
								<i class="icon-lock2 text-muted"></i>
							</div>
						</div>

						<div class="form-group login-options">
							<div class="row">
								<div class="col-sm-6">
									<label class="checkbox-inline">
										<input type="checkbox" class="styled" checked="checked">
										Remember
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-block" style="background-color: #5865f2; color: #fff; border-radius: 31px;">Login <i class="icon-circle-right2 position-right"></i></button>
						</div>
						<span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
					</div>
					
                    <script>
                        $(document).ready(function() {
                            $("#show_hide_password span").on('click', function(event) {
                                event.preventDefault();
                                if($('#show_hide_password input').attr("type") == "text") {
                                    $('#show_hide_password input').attr('type', 'password');
                                    $('#show_hide_password i').addClass( "icon-eye-blocked" );
                                    $('#show_hide_password i').removeClass( "icon-eye" );
                                } else if($('#show_hide_password input').attr("type") == "password") {
                                    $('#show_hide_password input').attr('type', 'text');
                                    $('#show_hide_password i').removeClass( "icon-eye-blocked" );
                                    $('#show_hide_password i').addClass( "icon-eye" );
                                }
                            });
                        });
                    </script>
				</form>
				<!-- /advanced login -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
