<?php

return [
    'layouts' => [
        'trending' => 'Sedang Tren',
        'navbar' => [
            'menu' => [
                'home' => 'Beranda',
                'news' => 'Berita',
                'events' => 'Acara',
                'about-us' => 'Tentang Kami',
                'contact' => 'Kontak Kami',
                'faq' => 'FAQ',
            ],
        ],
    ],
    
    'footer' => [
        'about-us' => 'Tentang Kami',
        'most-view-post' => 'Posting Paling Banyak Dilihat',
        'last-modified-post' => 'Posting Terbaru',
        'subscribe' => [
            'placeholder' => 'Masukkan email Anda',
            'button' => 'Langganan',
        ],
        'about-us' => 'Tentang Kami',
        'contact' => 'Kontak Kami',
        'faq' => 'FAQ',
    ],

    'landingpage' => [
        'trending' => 'Sedang Tren',
        'events' => 'Acara',
        'news' => 'Berita',
        'tags' => 'Tag',
        'most-popular' => 'Paling Populer',
        'show-more' => 'Tampilkan lebih banyak',
    ],

    'pages' => [
        'news' => [
            'title' => 'Berita',
        ],
        'event' => [
            'title' => 'Acara',
        ],
        'about-us' => [
            'title' => 'Tentang Kami',
            'events' => 'Acara',
        ],
        'contact-us' => [
            'title' => 'Kontak Kami',
            'call' => 'Hubungi kami',
            'questions' => 'Ada pertanyaan',
            'address' => 'Alamat',
        ],
        'faq' => [
            'title' => 'FAQ',
        ],
    ],

];