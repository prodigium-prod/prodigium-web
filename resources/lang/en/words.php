<?php

return [
    'layouts' => [
        'trending' => 'Trending',
        'navbar' => [
            'menu' => [
                'home' => 'Home',
                'news' => 'News',
                'events' => 'Events',
                'about-us' => 'About Us',
                'contact' => 'Contact Us',
                'faq' => 'FAQ',
            ],
        ],
    ],
    
    'footer' => [
        'about-us' => 'About Us',
        'most-view-post' => 'Most Viewed Posts',
        'last-modified-post' => 'Last Modified Posts',
        'subscribe' => [
            'placeholder' => 'Enter your email',
            'button' => 'Subscribe',
        ],
        'about-us' => 'About Us',
        'contact' => 'Contact Us',
        'faq' => 'FAQ',
    ],

    'landingpage' => [
        'trending' => 'Trending',
        'events' => 'Events',
        'news' => 'News',
        'tags' => 'Tags',
        'most-popular' => 'Most Popular',
        'show-more' => 'Show More',
    ],

    'pages' => [
        'news' => [
            'title' => 'News',
        ],
        'event' => [
            'title' => 'Events',
        ],
        'about-us' => [
            'title' => 'About Us',
            'events' => 'Events',
        ],
        'contact-us' => [
            'title' => 'Contact Us',
            'call' => 'Call us',
            'questions' => 'Have any questions',
            'address' => 'Address',
        ],
        'faq' => [
            'title' => 'FAQ',
        ],
    ],
];