<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Back\AuthController;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Back\PageSettingController;
use App\Http\Controllers\Back\TagController;
use App\Http\Controllers\Back\NewsController;
use App\Http\Controllers\Back\EventsController;
use App\Http\Controllers\Back\UserController;

use App\Http\Controllers\Front\PagesController;
use App\Http\Controllers\Front\SubscribeController;

// Back --------------------------------------------------------------
Route::get('administrator', [AuthController::class, 'loginPage'])->name('login');
Route::post('authentication-process', [AuthController::class, 'process'])->name('authentication.process');
Route::group(['prefix' => 'administrator/', 'middleware' => 'auth'], function () {
    Route::get('dashboard', [DashboardController::class, 'main'])->name('dashboard');
    
    Route::get('news/add', [NewsController::class, 'addNewsForm'])->name('news.add.form');
    Route::post('news/add/action', [NewsController::class, 'addNewsAction'])->name('news.add.form.action');
    Route::get('news/list', [NewsController::class, 'newsList'])->name('news.list');
    Route::get('news/list/change-status/{id}/{status}', [NewsController::class, 'setStatus'])->name('news.list.change.status');
    Route::get('news/list/detail/edit/{id}', [NewsController::class, 'editNewsForm'])->name('news.list.detail.edit');
    Route::post('news/list/detail/edit/action/{id}', [NewsController::class, 'editNewsAction'])->name('news.list.detail.edit.action');
    Route::post('news/list/detail/edit/banner/{id}', [NewsController::class, 'changeBanner'])->name('news.list.detail.edit.banner');
    Route::post('news/list/delete/{id}', [NewsController::class, 'newsDelete'])->name('news.list.delete');
    
    Route::get('events/add', [EventsController::class, 'addEventsForm'])->name('events.add.form');
    Route::post('events/add/action', [EventsController::class, 'addEventsAction'])->name('events.add.form.action');
    Route::get('events/list', [EventsController::class, 'enventList'])->name('events.list');
    Route::get('events/list/change-status/{id}/{status}', [EventsController::class, 'setStatus'])->name('events.list.change.status');
    Route::get('events/list/detail/edit/{id}', [EventsController::class, 'editEventsForm'])->name('events.list.detail.edit');
    Route::post('events/list/detail/edit/action/{id}', [EventsController::class, 'editEventsAction'])->name('events.list.detail.edit.action');
    Route::post('events/list/detail/edit/banner/{id}', [EventsController::class, 'changeBanner'])->name('events.list.detail.edit.banner');
    Route::post('events/list/delete/{id}', [EventsController::class, 'eventsDelete'])->name('events.list.delete');
    
    Route::get('users', [UserController::class, 'userList'])->name('page.user');
    Route::post('users/add', [UserController::class, 'userAdd'])->name('page.user.add');
    Route::post('users/update/{id}', [UserController::class, 'userUpdate'])->name('page.user.update');
    Route::post('users/delete/{id}', [UserController::class, 'userDelete'])->name('page.user.delete');

    Route::get('tags', [TagController::class, 'tagList'])->name('page.tag');
    Route::post('tags/add', [TagController::class, 'tagAdd'])->name('page.tag.add');
    Route::post('tags/update/{id}', [TagController::class, 'tagUpdate'])->name('page.tag.update');
    Route::post('tags/delete/{id}', [TagController::class, 'tagDelete'])->name('page.tag.delete');

    Route::get('page-settings', [PageSettingController::class, 'pageSettings'])->name('page.settings');
    Route::post('page-settings-save/{id}', [PageSettingController::class, 'pageSettingSave'])->name('page.settings.save');
    Route::post('page-settings/change-files/{id}/{type}', [PageSettingController::class, 'changeAssets'])->name('page.settings.change.file');

    Route::get('about-us', [PageSettingController::class, 'aboutUs'])->name('page.about-us');
    Route::post('about-us-save/{id}', [PageSettingController::class, 'aboutUsSave'])->name('page.about-us.save');
    Route::post('about-us/change-banner/{id}', [PageSettingController::class, 'changeBannerAboutUs'])->name('page.about-us.change.banner');

    Route::get('contact', [PageSettingController::class, 'contact'])->name('page.contact');
    Route::post('contact/{id}', [PageSettingController::class, 'contactSave'])->name('page.contact.save');

    Route::get('faq', [PageSettingController::class, 'faq'])->name('page.faq');
    Route::post('faq/add', [PageSettingController::class, 'faqAdd'])->name('page.faq.add');
    Route::post('faq/update/{id}', [PageSettingController::class, 'faqUpdate'])->name('page.faq.update');
    Route::post('faq/delete/{id}', [PageSettingController::class, 'faqDelete'])->name('page.faq.delete');

    Route::get('subscribe', [PageSettingController::class, 'subscribe'])->name('page.subscribe');

    // Start Session --------------------------------------------------------------------------------------
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    // End Session ----------------------------------------------------------------------------------------
});

// Front -------------------------------------------------------------
Route::get('/', function () {
    return view('front.index');
});

Route::middleware(['language'])->group(function () {
    Route::group(['prefix' => '/{lang}'], function () {
        Route::get('/home', [PagesController::class, 'main'])->name('main');
        Route::get('/product-services', [PagesController::class, 'productServices'])->name('productServices');
        Route::get('/news', [PagesController::class, 'news'])->name('news');
        Route::get('/news/{code}/{slug}', [PagesController::class, 'newsDetail'])->name('news-detail');
        Route::get('/events', [PagesController::class, 'events'])->name('events');
        Route::get('/events/{code}/{slug}', [PagesController::class, 'eventsDetail'])->name('events-detail');
        Route::get('/about-us', [PagesController::class, 'aboutUs'])->name('about-us');
        Route::get('/contact', [PagesController::class, 'contact'])->name('contact');
        Route::get('/faq', [PagesController::class, 'faq'])->name('faq');
        
        Route::post('/subscribe-now', [SubscribeController::class, 'subscribeNow'])->name('subscribe.now');
    });
});

// Route::get('/', function () {
//     return view('welcome');
// });
