<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->text('news_code')->nullable();
            $table->text('slug_id')->nullable();
            $table->text('slug_en')->nullable();
            $table->text('tag')->nullable();
            $table->text('meta_keyword')->nullable();
            $table->text('meta_description_id')->nullable();
            $table->text('meta_description_en')->nullable();
            $table->text('banner')->nullable();
            $table->text('title_id')->nullable();
            $table->text('title_en')->nullable();
            $table->text('content_id')->nullable();
            $table->text('content_en')->nullable();
            $table->text('wiews')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
