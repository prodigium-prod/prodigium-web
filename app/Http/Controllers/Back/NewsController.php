<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\News;
use App\Models\Tag;

use Str;
use Session;
use Validator;

class NewsController extends Controller
{
    public function newsList()
    {
        $news       = News::latest()->paginate(10);
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>News List</li>
        ";
        return view('back.page.news', compact('news', 'breadcrumb'));
    }

    public function setStatus($id, $status)
    {
        try {
            $news = News::find($id);
            $news->update(['status' => $status]);
            
            Session::flash('message_flash', 'Successfully to change ...');
            return redirect()->back(); 
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back();
        }
    }

    public function addNewsForm()
    {
        $tags       = Tag::all();
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Add News</li>
        ";
        return view('back.page.form-add-news', compact('tags', 'breadcrumb'));
    }
    
    public function addNewsAction(Request $request)
    {
        try {
            $news       = new News;

            $news_code  = date('dmyhis');
            $slug_id    = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_id));
            $slug_en    = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_en));
            $file_url   = "";
            $tags       = json_encode($request->tags);

            if ($request->has('file')) {
                $file_image = $request->file('file');
                $path_image = 'assets/images/news';
                $name_image = 'news-'.date('dmHis');
                
                if ($file_image->move($path_image,$name_image.".".$file_image->getClientOriginalExtension())) {
                    $file_url   = $path_image."/".$name_image.".".$file_image->getClientOriginalExtension();
                }

            } else {
                Session::flash('message_flash_failed', 'Banner is Required ...');
                return redirect()->back(); 
            }

            $news->news_code            = $news_code;
            $news->slug_id              = $slug_id;
            $news->slug_en              = $slug_en;
            $news->tag                  = $tags;
            $news->meta_keyword         = $request->keywords;
            $news->meta_description_id  = $request->description_id;
            $news->meta_description_en  = $request->description_en;
            $news->banner               = $file_url;
            $news->title_id             = $request->title_id;
            $news->title_en             = $request->title_en;
            $news->content_id           = $request->content_id;
            $news->content_en           = $request->content_en;
            $news->save();

            Session::flash('message_flash', 'Successfully to change ...');
            return redirect()->back(); 
        } catch (\Throwable $th) {
            // dd($th);
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }

    public function editNewsForm($id)
    {
        $news       = News::find($id);
        $news_tag   = json_decode($news->tag);
        $tags       = Tag::all();
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Edit News</li>
        ";
        return view('back.page.form-edit-news', compact('news', 'news_tag', 'tags', 'breadcrumb'));
    }
    
    public function changeBanner(Request $request, $id)
    {
        try {
            if ($request->has('file')) {
                $file_image = $request->file('file');
                $path_image = 'assets/images/news';
                $name_image = 'news-'.date('dmHis');
                $file_url   = "";
                $data       = array();

                if ($file_image->move($path_image,$name_image.".".$file_image->getClientOriginalExtension())) {
                    $file_url   = $path_image."/".$name_image.".".$file_image->getClientOriginalExtension();
                }

                $news = News::find($id);

                if(file_exists($news->banner)) {
                    unlink($news->banner);
                }
                $data = [
                    'banner' => $file_url
                ];

                if ($news->update($data)) {
                    Session::flash('message_flash', 'Successfully changed the banner ...');
                    return redirect()->back(); 
                } else {
                    Session::flash('message_flash_failed', 'Failed changed the banner ...');
                    return redirect()->back(); 
                }
            } else {
                Session::flash('message_flash_failed', 'Failed changed the banner ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed changed the banner ...');
            return redirect()->back(); 
        }
    }
    
    public function editNewsAction(Request $request, $id)
    {
        try {
            $news       = News::find($id);

            $slug_id    = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_id));
            $slug_en    = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_en));
            $tags       = json_encode($request->tags);

            $news->slug_id              = $slug_id;
            $news->slug_en              = $slug_en;
            $news->tag                  = $tags;
            $news->meta_keyword         = $request->keywords;
            $news->meta_description_id  = $request->description_id;
            $news->meta_description_en  = $request->description_en;
            $news->title_id             = $request->title_id;
            $news->title_en             = $request->title_en;
            $news->content_id           = $request->content_id;
            $news->content_en           = $request->content_en;
            $news->save();

            Session::flash('message_flash', 'Successfully to change ...');
            return redirect()->back(); 
        } catch (\Throwable $th) {
            // dd($th);
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }
    
    public function newsDelete($id)
    {
        try {
            $news   = News::find($id);
            $banner = $news->banner;

            if ($news->delete()) {
                if(file_exists($banner)) {
                    unlink($banner);
                }
                
                Session::flash('message_flash', 'Successfully deleted data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to delete data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to delete data ...');
            return redirect()->back(); 
        }
    }
}
