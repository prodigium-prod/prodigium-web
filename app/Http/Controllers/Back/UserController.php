<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

use Session;
use Validator;

class UserController extends Controller
{
    public function userList()
    {
        $users       = User::paginate(10);
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>User List</li>
        ";
        return view('back.page.users', compact('users', 'breadcrumb'));
    }

    public function userAdd(Request $request)
    {
        try {
            $User           = new User;
            $User->role     = 1;
            $User->name     = $request->name;
            $User->email    = $request->email;
            $User->password = Hash::make($request->password);

            if ($User->save()) {
                Session::flash('message_flash', 'Successfully saved data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to save data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to save data ...');
            return redirect()->back(); 
        }
    }

    public function userUpdate(Request $request, $id)
    {
        try {
            $User    = User::find($id);

            if ($request->password == "secret") {
                $data   = [
                    'name'      => $request->name,
                    'email'     => $request->email,
                ];
            } else {
                $data   = [
                    'name'      => $request->name,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ];
            }

            if ($User->update($data)) {
                Session::flash('message_flash', 'Successfully updated data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to update data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to update data ...');
            return redirect()->back(); 
        }
    }
    
    public function userDelete($id)
    {
        try {
            $User    = User::find($id);

            if ($User->delete()) {
                Session::flash('message_flash', 'Successfully deleted data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to delete data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to delete data ...');
            return redirect()->back(); 
        }
    }
}
