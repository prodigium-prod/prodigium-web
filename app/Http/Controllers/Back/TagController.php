<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tag;

use Session;
use Validator;

class TagController extends Controller
{
    public function tagList()
    {
        $tags       = Tag::latest()->paginate(10);
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Tags</li>
        ";
        return view('back.page.tags', compact('tags', 'breadcrumb'));
    }

    public function tagAdd(Request $request)
    {
        try {
            $Tag        = new Tag;
            $Tag->tag   = $request->tag;

            if ($Tag->save()) {
                Session::flash('message_flash', 'Successfully saved data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to save data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to save data ...');
            return redirect()->back(); 
        }
    }

    public function tagUpdate(Request $request, $id)
    {
        try {
            $Tag    = Tag::find($id);
            $data   = [
                'tag' => $request->tag,
            ];

            if ($Tag->update($data)) {
                Session::flash('message_flash', 'Successfully updated data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to update data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to update data ...');
            return redirect()->back(); 
        }
    }
    
    public function tagDelete($id)
    {
        try {
            $Tag    = Tag::find($id);

            if ($Tag->delete()) {
                Session::flash('message_flash', 'Successfully deleted data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to delete data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to delete data ...');
            return redirect()->back(); 
        }
    }
}
