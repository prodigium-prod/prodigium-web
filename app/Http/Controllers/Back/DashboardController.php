<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\News;
use App\Models\Tag;

class DashboardController extends Controller
{
    public function main()
    {
        $news       = News::latest('views')->limit(5)->get();
        $news_total = News::count();
        $tag_total  = Tag::count();
        $breadcrumb         = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
        ";
        return view('back.page.dashboard', compact('news', 'news_total', 'tag_total', 'breadcrumb'));
    }
}
