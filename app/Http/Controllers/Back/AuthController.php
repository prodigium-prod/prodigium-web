<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\Models\User;

class AuthController extends Controller
{
    public function loginPage()
    {
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            return redirect()->route('dashboard'); 
        }
        
        return view('back.auth.login');
    }
    
    public function process(Request $request)
    {
        $rules = [
            'email'                 => 'required|email',
            'password'              => 'required|string'
        ];
  
        $messages = [
            'email.required'        => 'Email is required',
            'email.email'           => 'Email is required',
            'password.required'     => 'Password is required',
            'password.string'       => 'The password must be a string'
        ];
  
        $validator = Validator::make($request->all(), $rules, $messages);
  
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
  
        $data = [
            'email'     => $request->email,
            'password'  => $request->password,
        ];
  
        Auth::attempt($data);
  
        if (Auth::check()) { 
            //Login Success
            Session::flash('message_flash', 'Login successful, Access you as administrator ...');
            return redirect()->route('dashboard'); 
        } else { 
            //Login Fail
            Session::flash('error', 'Email or password incorect');
            return redirect()->back()->withInput();
        }
    }
    
    public function logout()
    {
        Auth::logout(); // menghapus session yang aktif

        if (isset($_GET['failed'])) {
            Session::flash('error', 'Email or password incorect');
        }
        return redirect()->route('login');
    }
}
