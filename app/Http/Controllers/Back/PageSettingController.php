<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PageSetting;
use App\Models\AboutUs;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Subscribe;

use Session;
use Validator;

class PageSettingController extends Controller
{
    public function pageSettings()
    {
        $pages      = PageSetting::latest()->first();
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Page Settings</li>
        ";
        return view('back.page.page-settings', compact('pages', 'breadcrumb'));
    }

    public function pageSettingSave(Request $request, $id)
    {
        try {
            $pageSetting    = PageSetting::find($id);
            $data           = [
                'headline'          => $request->headline,
                'meta_title'        => $request->meta_title,
                'meta_keywords'     => $request->meta_keywords,
                'meta_descriptions' => $request->meta_descriptions,
                'logo_alt'          => $request->logo_alt,
                'logo_descriptions' => $request->logo_descriptions,
                'version_code'      => $request->version_code,
                'version_detail'    => $request->version_detail,
            ];

            if ($pageSetting->update($data)) {
                Session::flash('message_flash', 'Successfully to change ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to change ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }

    public function changeAssets(Request $request, $id, $type)
    {
        try {
            if ($request->has('file')) {
                $file_image = $request->file('file');
                $path_image = 'assets/images/'.$type;
                $name_image = $type.'-'.date('dmHis');
                $file_url   = "";
                $status     = "false";
                $data       = array();

                if ($file_image->move($path_image,$name_image.".".$file_image->getClientOriginalExtension())) {
                    $file_url   = $path_image."/".$name_image.".".$file_image->getClientOriginalExtension();
                }

                $pageSetting        = PageSetting::find($id);

                switch ($type) {
                    case 'icon':
                        if(file_exists($pageSetting->meta_icon)) {
                            unlink($pageSetting->meta_icon);
                        }
                        $data = [
                            'meta_icon' => $file_url
                        ];
                        break;

                    case 'logo':
                        if(file_exists($pageSetting->logo_image)) {
                            unlink($pageSetting->logo_image);
                        }
                        $data = [
                            'logo_image' => $file_url
                        ];
                        break;
                    
                    default:
                        break;
                }

                if ($pageSetting->update($data)) {
                    Session::flash('message_flash', 'Successfully changed the image ...');
                    return redirect()->back(); 
                } else {
                    Session::flash('message_flash_failed', 'Failed changed the image ...');
                    return redirect()->back(); 
                }
            } else {
                Session::flash('message_flash_failed', 'Failed changed the image ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed changed the image ...');
            return redirect()->back(); 
        }
    }

    public function aboutUs()
    {
        $aboutUs      = AboutUs::latest()->first();
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>About Us</li>
        ";
        return view('back.page.about-us', compact('aboutUs', 'breadcrumb'));
    }
    
    public function aboutUsSave(Request $request, $id)
    {
        try {
            $aboutUs    = AboutUs::find($id);
            $data           = [
                'simple_en'          => $request->simple_en,
                'simple_id'        => $request->simple_id,
                'content_en'          => $request->content_en,
                'content_id'        => $request->content_id,
            ];

            if ($aboutUs->update($data)) {
                Session::flash('message_flash', 'Successfully to change ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to change ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }
    
    public function changeBannerAboutUs(Request $request, $id)
    {
        try {
            if ($request->has('file')) {
                $file_image = $request->file('file');
                $path_image = 'assets/images/banner';
                $name_image = 'banner-'.date('dmHis');
                $file_url   = "";
                $status     = "false";
                $data       = array();

                if ($file_image->move($path_image,$name_image.".".$file_image->getClientOriginalExtension())) {
                    $file_url   = $path_image."/".$name_image.".".$file_image->getClientOriginalExtension();
                }

                $aboutUs = AboutUs::find($id);

                if(file_exists($aboutUs->banner)) {
                    unlink($aboutUs->banner);
                }

                $data = [
                    'banner' => $file_url
                ];

                if ($aboutUs->update($data)) {
                    Session::flash('message_flash', 'Successfully changed the banner ...');
                    return redirect()->back(); 
                } else {
                    Session::flash('message_flash_failed', 'Failed changed the banner ...');
                    return redirect()->back(); 
                }
            } else {
                Session::flash('message_flash_failed', 'Failed changed the banner ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed changed the banner ...');
            return redirect()->back(); 
        }
    }

    public function contact()
    {
        $contact      = Contact::latest()->first();
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Contact</li>
        ";
        return view('back.page.contact', compact('contact', 'breadcrumb'));
    }
    
    public function contactSave(Request $request, $id)
    {
        try {
            $contact    = Contact::find($id);
            $data           = [
                'linkedin'      => $request->linkedin,
                'google'        => $request->google,
                'twitter'       => $request->twitter,
                'instagram'     => $request->instagram,
                'facebook'      => $request->facebook,
                'youtube'       => $request->youtube,
                'phone_number'  => $request->phone_number,
                'email'         => $request->email,
                'address'       => $request->address,
                'maps_embed'    => $request->maps_embed,
            ];

            if ($contact->update($data)) {
                Session::flash('message_flash', 'Successfully to change ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to change ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }
    
    public function faq()
    {
        $faqs       = Faq::latest()->paginate(10);
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>FAQ</li>
        ";
        return view('back.page.faq', compact('faqs', 'breadcrumb'));
    }

    public function faqAdd(Request $request)
    {
        try {
            $Faq                    = new Faq;
            $Faq->title_id          = $request->title_id;
            $Faq->title_en          = $request->title_en;
            $Faq->description_id    = $request->description_id;
            $Faq->description_en    = $request->description_en;

            if ($Faq->save()) {
                Session::flash('message_flash', 'Successfully saved data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to save data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to save data ...');
            return redirect()->back(); 
        }
    }

    public function faqUpdate(Request $request, $id)
    {
        try {
            $faq    = Faq::find($id);
            $data           = [
                'title_id'          => $request->title_id,
                'title_en'          => $request->title_en,
                'description_id'    => $request->description_id,
                'description_en'    => $request->description_en
            ];

            if ($faq->update($data)) {
                Session::flash('message_flash', 'Successfully updated data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to update data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to update data ...');
            return redirect()->back(); 
        }
    }
    
    public function faqDelete($id)
    {
        try {
            $faq    = Faq::find($id);

            if ($faq->delete()) {
                Session::flash('message_flash', 'Successfully deleted data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to delete data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to delete data ...');
            return redirect()->back(); 
        }
    }
    
    public function subscribe()
    {
        $subscribe  = Subscribe::latest()->paginate(10);
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Subscribe</li>
        ";
        return view('back.page.subscribe', compact('subscribe', 'breadcrumb'));
    }
}
