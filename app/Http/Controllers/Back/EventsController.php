<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Events;
use App\Models\Tag;

use Str;
use Session;
use Validator;

class EventsController extends Controller
{
    public function enventList()
    {
        $events       = Events::latest()->paginate(10);
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Event List</li>
        ";
        return view('back.page.events', compact('events', 'breadcrumb'));
    }

    public function setStatus($id, $status)
    {
        try {
            $event = Events::find($id);
            $event->update(['status' => $status]);
            
            Session::flash('message_flash', 'Successfully to change ...');
            return redirect()->back(); 
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back();
        }
    }

    public function addEventsForm()
    {
        $tags       = Tag::all();
        $breadcrumb = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Add Event</li>
        ";
        return view('back.page.form-add-events', compact('tags', 'breadcrumb'));
    }
    
    public function addEventsAction(Request $request)
    {
        try {
            $event         = new Events;

            $event_code     = date('dmyhis');
            $slug_id        = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_id));
            $slug_en        = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_en));
            $file_url       = "";
            $tags           = json_encode($request->tags);

            if ($request->has('file')) {
                $file_image = $request->file('file');
                $path_image = 'assets/images/events';
                $name_image = 'events-'.date('dmHis');
                
                if ($file_image->move($path_image,$name_image.".".$file_image->getClientOriginalExtension())) {
                    $file_url   = $path_image."/".$name_image.".".$file_image->getClientOriginalExtension();
                }

            } else {
                Session::flash('message_flash_failed', 'Banner is Required ...');
                return redirect()->back(); 
            }

            $event->event_code          = $event_code;
            $event->slug_id             = $slug_id;
            $event->slug_en             = $slug_en;
            $event->tag                 = $tags;
            $event->meta_keyword        = $request->keywords;
            $event->meta_description_id = $request->description_id;
            $event->meta_description_en = $request->description_en;
            $event->banner              = $file_url;
            $event->title_id            = $request->title_id;
            $event->title_en            = $request->title_en;
            $event->content_id          = $request->content_id;
            $event->content_en          = $request->content_en;
            $event->save();

            Session::flash('message_flash', 'Successfully to change ...');
            return redirect()->back(); 
        } catch (\Throwable $th) {
            dd($th);
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }

    public function editEventsForm($id)
    {
        $events         = Events::find($id);
        $events_tag     = json_decode($events->tag);
        $tags           = Tag::all();
        $breadcrumb     = "
            <li><a href='".route('dashboard')."'><i class='icon-home2 position-left'></i> Dashboard</a></li>
            <li class='active'>Edit Event</li>
        ";
        return view('back.page.form-edit-events', compact('events', 'events_tag', 'tags', 'breadcrumb'));
    }
    
    public function changeBanner(Request $request, $id)
    {
        try {
            if ($request->has('file')) {
                $file_image = $request->file('file');
                $path_image = 'assets/images/events';
                $name_image = 'events-'.date('dmHis');
                $file_url   = "";
                $data       = array();

                if ($file_image->move($path_image,$name_image.".".$file_image->getClientOriginalExtension())) {
                    $file_url   = $path_image."/".$name_image.".".$file_image->getClientOriginalExtension();
                }

                $event = Events::find($id);

                if(file_exists($event->banner)) {
                    unlink($event->banner);
                }
                $data = [
                    'banner' => $file_url
                ];

                if ($event->update($data)) {
                    Session::flash('message_flash', 'Successfully changed the banner ...');
                    return redirect()->back(); 
                } else {
                    Session::flash('message_flash_failed', 'Failed changed the banner ...');
                    return redirect()->back(); 
                }
            } else {
                Session::flash('message_flash_failed', 'Failed changed the banner ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed changed the banner ...');
            return redirect()->back(); 
        }
    }
    
    public function editEventsAction(Request $request, $id)
    {
        try {
            $event       = Events::find($id);

            $slug_id    = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_id));
            $slug_en    = Str::replace([' ', '.', ',', '?', '!', '(', ')'], ['-', '', '', '', '', '', ''], Str::lower($request->title_en));
            $tags       = json_encode($request->tags);

            $event->slug_id              = $slug_id;
            $event->slug_en              = $slug_en;
            $event->tag                  = $tags;
            $event->meta_keyword         = $request->keywords;
            $event->meta_description_id  = $request->description_id;
            $event->meta_description_en  = $request->description_en;
            $event->title_id             = $request->title_id;
            $event->title_en             = $request->title_en;
            $event->content_id           = $request->content_id;
            $event->content_en           = $request->content_en;
            $event->save();

            Session::flash('message_flash', 'Successfully to change ...');
            return redirect()->back(); 
        } catch (\Throwable $th) {
            // dd($th);
            Session::flash('message_flash_failed', 'Failed to change ...');
            return redirect()->back(); 
        }
    }
    
    public function eventsDelete($id)
    {
        try {
            $event   = Events::find($id);
            $banner = $event->banner;

            if ($event->delete()) {
                if(file_exists($banner)) {
                    unlink($banner);
                }
                
                Session::flash('message_flash', 'Successfully deleted data ...');
                return redirect()->back(); 
            } else {
                Session::flash('message_flash_failed', 'Failed to delete data ...');
                return redirect()->back(); 
            }
        } catch (\Throwable $th) {
            Session::flash('message_flash_failed', 'Failed to delete data ...');
            return redirect()->back(); 
        }
    }
}
