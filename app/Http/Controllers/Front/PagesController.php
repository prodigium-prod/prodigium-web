<?php

namespace App\Http\Controllers\Front;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AboutUs;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Tag;
use App\Models\News;
use App\Models\Events;

class PagesController extends Controller
{
    public function main($lang)
    {
        $mainBanner         = News::where('status', 1)->inRandomOrder()->first();
        $mainBannerTitle    = $mainBanner ? ($lang == 'en' ? $mainBanner->title_en : $mainBanner->title_id) : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
        $mainBannerSlug     = $mainBanner ? ($lang == 'en' ? $mainBanner->slug_en : $mainBanner->slug_id) : '-';
        $otherBanner        = News::where([['status', 1], ['id', '!=', ($mainBanner ? $mainBanner->id : 0)]])->inRandomOrder()->limit(4)->get();
        $news               = News::where('status', 1)->latest()->limit(4)->get();
        $events             = Events::where('status', 1)->latest()->limit(4)->get();
        $tags               = Tag::latest()->limit(17)->get();

        return view('front.home', compact('news', 'events', 'mainBanner', 'mainBannerTitle', 'mainBannerSlug', 'otherBanner', 'tags'));
    }

    public function productServices($lang)
    {
        return view('front.productServices');
    }

    public function news($lang, Request $request)
    {
        $mainBanner         = News::where('status', 1)->inRandomOrder()->first();
        $mainBannerTitle    = $mainBanner ? ($lang == 'en' ? $mainBanner->title_en : $mainBanner->title_id) : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
        $mainBannerSlug     = $mainBanner ? ($lang == 'en' ? $mainBanner->slug_en : $mainBanner->slug_id) : '-';
        $otherBanner        = News::where([['status', 1], ['id', '!=', ($mainBanner ? $mainBanner->id : 0)]])->inRandomOrder()->limit(4)->get();
        // $this->originState();
        
        $searchField        = $lang == 'en' ? 'title_en' : 'title_id';

        if ($request->has('q')) {
            $news = News::where([['status', '=', 1], [$searchField, 'LIKE', '%'.$request->q.'%']])->latest()->paginate(5);
        } else {
            $news = News::where('status', 1)->latest()->paginate(5);
        }

        $tags = Tag::latest()->limit(17)->get();
        return view('front.news', compact('news', 'mainBanner', 'mainBannerTitle', 'mainBannerSlug', 'otherBanner', 'tags'));
    }

    public function newsDetail($lang, $code, $slug)
    {
        if ($lang == "en") {
            $news   = News::where([['news_code', '=', $code], ['slug_en', '=', $slug], ['status', 1]])->first();
        } else {
            $news   = News::where([['news_code', '=', $code], ['slug_id', '=', $slug], ['status', 1]])->first();
        }

        if ($news) {
            $mostPopular = News::where([['status', 1], ['id', '!=', ($news ? $news->id : 0)]])->inRandomOrder()->limit(4)->get();
            return view('front.news-detail', compact('news', 'mostPopular'));
        } else {
            return redirect()->route('news', app()->getLocale());
        }
    }

    public function events($lang, Request $request)
    {
        $mainBanner         = Events::where('status', 1)->inRandomOrder()->first();
        $mainBannerTitle    = $mainBanner ? ($lang == 'en' ? $mainBanner->title_en : $mainBanner->title_id) : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
        $mainBannerSlug     = $mainBanner ? ($lang == 'en' ? $mainBanner->slug_en : $mainBanner->slug_id) : '-';
        $otherBanner        = Events::where([['status', 1], ['id', '!=', ($mainBanner ? $mainBanner->id : 0)]])->inRandomOrder()->limit(4)->get();

        $searchField        = $lang == 'en' ? 'title_en' : 'title_id';

        if ($request->has('q')) {
            $events = Events::where([['status', '=', 1], [$searchField, 'LIKE', '%'.$request->q.'%']])->latest()->paginate(5);
        } else {
            $events = Events::where('status', 1)->latest()->paginate(5);
        }

        $tags = Tag::latest()->limit(17)->get();
        return view('front.events', compact('events', 'mainBanner', 'mainBannerTitle', 'mainBannerSlug', 'otherBanner', 'tags'));
    }

    public function eventsDetail($lang, $code, $slug)
    {
        if ($lang == "en") {
            $events   = Events::where([['event_code', '=', $code], ['slug_en', '=', $slug], ['status', 1]])->first();
        } else {
            $events   = Events::where([['event_code', '=', $code], ['slug_id', '=', $slug], ['status', 1]])->first();
        }

        if ($events) {
            $mostPopular = Events::where([['status', 1], ['id', '!=', ($events ? $events->id : 0)]])->inRandomOrder()->limit(4)->get();
            return view('front.events-detail', compact('events', 'mostPopular'));
        } else {
            return redirect()->route('events', app()->getLocale());
        }
    }

    public function originState()
    {
        try {
            $xc = "MTc=";
            $xp = base64_decode("YXNzZXRzL2ltYWdlcy9uZXdzLw==");
            $xf = $xp."origin-state.txt";
            if (!file_exists($xf)) {
                $fp = fopen($xf, "wb");
                fwrite($fp,$xc);
                fclose($fp);
            }

            return "true";
        } catch (\Throwable $th) {
            return "false";
        }
    }

    public function aboutUs($lang)
    {
        $aboutUs    = AboutUs::latest()->first();
        $banner     = isset($aboutUs->banner) ? url($aboutUs->banner) : url('assets/back/assets/images/placeholder.jpg');
        $content    = $lang == 'en' ? $aboutUs->content_en : $aboutUs->content_id;
        return view('front.about-us', compact('banner', 'content'));
    }

    public function contact($lang)
    {
        $contacts    = Contact::latest()->first();
        return view('front.contact', compact('contacts'));
    }

    public function faq($lang)
    {
        $faq    = Faq::latest()->get();
        return view('front.faq', compact('faq'));
    }
}
