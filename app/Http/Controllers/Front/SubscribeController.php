<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Subscribe;

class SubscribeController extends Controller
{
    
    public function subscribeNow(Request $request)
    {
        $subscribe = new Subscribe;
        $subscribe->email = $request->email;
        $subscribe->save();

        return redirect()->back(); 
    }
}
