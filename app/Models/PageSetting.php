<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'meta_title', 'meta_icon', 'meta_keywords', 'meta_descriptions', 'headline', 'logo_image', 'logo_alt', 'logo_descriptions', 'advance_attributes', 'version_code', 'version_detail'
    ];
}
