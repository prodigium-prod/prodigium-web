<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Tag;

class News extends Model
{
    use HasFactory;
    protected $fillable = [
        'news_code', 'slug_id', 'slug_en', 'tag', 'meta_keyword', 'meta_description_id', 'meta_description_en', 'banner', 'title_id', 'title_en', 'content_id', 'content_en', 'views', 'status'
    ];

    public function slug($lang)
    {
        if ($lang == "en") {
            return $this->slug_en;
        } else {
            return $this->slug_id;
        }
    }

    public function description($lang)
    {
        if ($lang == "en") {
            return $this->meta_description_en;
        } else {
            return $this->meta_description_id;
        }
    }

    public function title($lang)
    {
        if ($lang == "en") {
            return $this->title_en;
        } else {
            return $this->title_id;
        }
    }

    public function content($lang)
    {
        if ($lang == "en") {
            return $this->content_en;
        } else {
            return $this->content_id;
        }
    }

    public function tags()
    {
        $tags   = json_decode($this->tag);
        $result = array();
        $x      = 0;
        foreach ($tags as $key => $value) {
            $fetch_tag = Tag::find($value);
            if ($fetch_tag) {
                $result[$x] = $fetch_tag->tag;

                $x += 1;
            }
        }
        return $result;
    }
}
