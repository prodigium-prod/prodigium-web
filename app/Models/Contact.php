<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    protected $fillable = [
        'linkedin', 'google', 'twitter', 'instagram', 'facebook', 'youtube', 'phone_number', 'email', 'address', 'maps_embed'
    ];
}
