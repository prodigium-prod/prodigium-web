<?php
    use App\Models\PageSetting;
    use App\Models\AboutUs;
    use App\Models\Contact;
    use App\Models\News;

    // Generata number of item
    function x_number($curnPage = 0, $perPage = 0) {
        if ($curnPage == 1) {
            $no = 1;
        } else {
            $no = $perPage * $curnPage - ($perPage - 1);
        }

        return $no;
    }

    function pageSetup()
    {
        $page = PageSetting::latest()->first();

        if ($page) {
            return $page;
        } else {
            return null;
        }
    }

    function fetchContact()
    {
        $contact = Contact::latest()->first();

        if ($contact) {
            return $contact;
        } else {
            return null;
        }
    }
    
    function fetchAbout()
    {
        $about = AboutUs::latest()->first();

        if ($about) {
            return $about;
        } else {
            return null;
        }
    }

    function fetchNews($limit = 4)
    {
        $news = News::where('status', 1)->inRandomOrder()->limit($limit)->get();
        return $news;
    }
?>